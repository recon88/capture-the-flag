package de.wrenchbox.ctf.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

public class Performance {

	private static HashMap<String, Performance> references = new HashMap<String, Performance>();
	private ArrayList<Long> times = new ArrayList<Long>();
	private String key;
	private long start = 0;

	private Performance() {}

	private Performance(String key) {
		this.key = key;
	}

	public static Performance getInstance(String key) {
		if(references.get(key) == null) { 
			Performance p = new Performance(key);
			references.put(key, p);
			return p;
		} else {
			return references.get(key);
		}	
	}

	public static Set<String> keySet() {
		return references.keySet();
	}

	public static long getTotal() {
		long total = 0;
		for(String key : keySet()) {
			total += references.get(key).getSum();
		}
		return total;
	}

	public void startTest() throws IllegalStateException {
		if(start == 0) {
			start = System.nanoTime();
		} else {
			resetTest();
			throw new IllegalStateException("Test for '"+key+"' already started.");
		}
	}

	public void stopTest() throws IllegalStateException {
		if(start != 0) {
			long time = System.nanoTime()-start;
			times.add(time);
			start = 0;
		} else {
			throw new IllegalStateException("Test for '"+key+"' not yet started.");
		}
	}

	public void resetTest() {
		start = 0;
	}

	public long getSum() {
		long sum = 0;
		for(long l : times) {
			sum += l;
		}
		return sum == 0 ? 1 : sum;
	}

	public long getAvg() {
		if(times.size() != 0)
			return getSum()/times.size();
		else
			return 0;
	}

	public float getRecent() {
		long recent = 0;
		for(int i = times.size()-1; i >= Math.floor(times.size()*0.9); i--) {
			recent += times.get(i);
		}
		return recent;
	}

	public static void stopAll() {
		for(Entry<String, Performance> e : references.entrySet()) {
			e.getValue().resetTest();
		}
	}
}
