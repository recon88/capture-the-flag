package de.wrenchbox.ctf.Util;

import java.util.HashMap;
import java.util.Map.Entry;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import de.wrenchbox.ctf.Config.ConfigHandler;
import de.wrenchbox.ctf.Game;
import de.wrenchbox.ctf.Team;

public class Messages {

	private static HashMap<String, String> replace = new HashMap<String, String>();

	public static String colorizeText(String string) {
		return ChatColor.translateAlternateColorCodes('&', string);
	}

	public static void put(String key, String value) {
		replace.put(key, value);
	}

	public static void sendBroadcast(String configmessage) {
		String message = ConfigHandler.getMsgConfig().getString("broadcast-messages." + configmessage);
		for(Entry<String, String> e : replace.entrySet()) {
			message = message.replace(e.getKey(), e.getValue());
		}
		replace.clear();
		Game.getInstance().sendMessage(colorizeText(message));
	}

	public static void sendPlayer(String configmessage, CommandSender player) {
		String message = ConfigHandler.getMsgConfig().getString("player-messages." + configmessage);
		for(Entry<String, String> e : replace.entrySet()) {
			message = message.replace(e.getKey(), e.getValue());
		}
		replace.clear();
		player.sendMessage(colorizeText(message));
	}

	public static void sendTeam(String configmessage, Team team) {
		String message = ConfigHandler.getMsgConfig().getString("team-messages." + configmessage);
		for(Entry<String, String> e : replace.entrySet()) {
			message = message.replace(e.getKey(), e.getValue());
		}
		replace.clear();
		team.sendMessage(colorizeText(message));
	}

	public static String get(String configmessage) {
		String message = ConfigHandler.getMsgConfig().getString("general-messages." + configmessage);
		return message;
	}
}
