package de.wrenchbox.ctf.Util;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.metadata.Metadatable;

import de.wrenchbox.ctf.CaptureTheFlag;
import de.wrenchbox.ctf.Game;
import de.wrenchbox.ctf.Team;

public class Meta {

	public static void setMetadata(Metadatable obj, String key, Object value) {
		obj.setMetadata(key,new FixedMetadataValue(CaptureTheFlag.getPlugin(), value));
	}

	public static MetadataValue getMetadata(Metadatable obj, String key){
		for(MetadataValue value : obj.getMetadata(key)){
			if(value.getOwningPlugin().getDescription().getName().equals(CaptureTheFlag.getPlugin().getDescription().getName())){
				return value;
			}
		}
		return null;
	}

	public static void removeMetadata(Metadatable obj, String key) {
		obj.removeMetadata(key, CaptureTheFlag.getPlugin());
	}

	public static void protect(Block block) {
		setMetadata(block, "protected", true);
	}

	public static void unprotect(Block block) {
		removeMetadata(block, "protected");
	}

	public static boolean isProtected(Block block) {
		return getMetadata(block, "protected") == null ? false : getMetadata(block, "protected").asBoolean();
	}

	public static Player getAncient(Team t) {
		Team u = t.getEnemyTeam();
		if(Game.getInstance().isCTF()
				&& Meta.getMetadata(u.getFlag(), "position") != null
				&& Meta.getMetadata(u.getFlag(), "position").value() instanceof Player) {
			return (Player) Meta.getMetadata(u.getFlag(), "position").value();
		} else {
			return null;
		}
	}

	public static boolean isAncient(Player p) {
		Team t = Team.getTeamByPlayer(p);
		return getAncient(t) != null && p.equals(getAncient(t));
	}

	public static boolean isFlag(Block block) {
		for(Team t : Game.getInstance().getTeams()) {
			if(t.getFlag() != null && Meta.getMetadata(t.getFlag(), "position") != null
					&& Meta.getMetadata(t.getFlag(), "position").value() instanceof Block
					&& ((Block) Meta.getMetadata(t.getFlag(), "position").value()).equals(block)) {
				return true;
			}
		}
		return false;
	}

	public static boolean isAlive(Player player) {
		int respawnDelay = CaptureTheFlag.getPlugin().getConfig().getInt("settings.respawn");
		return getMetadata(player, "death") == null || System.currentTimeMillis() >= getMetadata(player, "death").asLong()+respawnDelay*1000;
	}

	public static void hide(Player player) {
		setMetadata(player, "invisible", true);
	}

	public static void show(Player player) {
		removeMetadata(player, "invisible");
	}

	public static boolean isInvisible(Player player) {
		return Meta.getMetadata(player, "invisible") != null && Meta.getMetadata(player, "invisible").asBoolean();
	}

	public static boolean onCooldown(Player player, String name) {
		String id = "cd_"+name;
		return Meta.getMetadata(player, id) != null && System.currentTimeMillis() < Meta.getMetadata(player, id).asLong();
	}

	public static void setCooldown(Player player, String name, long cd) {
		String id = "cd_"+name;
		setMetadata(player, id, System.currentTimeMillis()+cd);
	}

	public static long getCooldown(Player player, String name) {
		String id = "cd_"+name;
		return getMetadata(player, id).asLong()-System.currentTimeMillis();
	}

}
