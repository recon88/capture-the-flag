package de.wrenchbox.ctf.Util;

import java.util.logging.Logger;

import lib.PatPeter.SQLibrary.DBMS;
import lib.PatPeter.SQLibrary.Database;
import lib.PatPeter.SQLibrary.Factory.DatabaseConfig;
import lib.PatPeter.SQLibrary.Factory.DatabaseFactory;
import lib.PatPeter.SQLibrary.Factory.InvalidConfigurationException;
import lib.PatPeter.SQLibrary.Factory.DatabaseConfig.Parameter;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;

import de.wrenchbox.ctf.CaptureTheFlag;

public class SQL {

	public static Database createDatabase() {
		Database db = null;
		try {
			DatabaseConfig dbconfig = new DatabaseConfig();
			FileConfiguration config = CaptureTheFlag.getPlugin().getConfig();

			dbconfig.setLog(Logger.getLogger("SQL"));

			if(config.getBoolean("database.use-mysql")) {
				dbconfig.setType(DBMS.MySQL);
				dbconfig.setParameter(Parameter.HOSTNAME, config.getString("database.hostname", "localhost"));
				dbconfig.setParameter(Parameter.DATABASE, config.getString("database.database"));
				dbconfig.setParameter(Parameter.PORTNMBR, config.getString("database.port", "3306"));
				dbconfig.setParameter(Parameter.USERNAME, config.getString("database.user"));
				dbconfig.setParameter(Parameter.PASSWORD, config.getString("database.password"));
			} else {
				dbconfig.setType(DBMS.SQLite);
				dbconfig.setParameter(Parameter.LOCATION, CaptureTheFlag.getPlugin().getDataFolder().getAbsolutePath());
				dbconfig.setParameter(Parameter.FILENAME, "scores");
			}
			dbconfig.setParameter(Parameter.PREFIX, "CTF");

			db = DatabaseFactory.createDatabase(dbconfig);
		} catch (InvalidConfigurationException e) {
			Bukkit.getLogger().severe("Error while accessing database. Score cannot be saved: "+e.getMessage());
		}

		return db;
	}
}
