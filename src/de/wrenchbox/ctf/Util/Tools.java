package de.wrenchbox.ctf.Util;

import org.bukkit.Location;

import de.wrenchbox.ctf.Game;
import de.wrenchbox.ctf.Team;

public class Tools {

	public static double distance2DSquared(Location l, Location m) {
		return Math.pow(l.getBlockX()-m.getBlockX(), 2)+Math.pow(l.getBlockZ()-m.getBlockZ(), 2);
	}

	public static Location getRandomLocation(Location l, int min, int max) {
		int diff = max - min;
		double x, z;
		if((int) (Math.random()*2) == 0) {
			x = Math.pow(-1, (int) (Math.random()*2))*(Math.random()*diff + min);
			z = -max+2*Math.random()*max;
		} else {
			x = -max+2*Math.random()*max;
			z = Math.pow(-1, (int) (Math.random()*2))*(Math.random()*diff + min);
		}

		return l.getWorld().getHighestBlockAt(l.clone().add(x, 0, z)).getLocation();
	}

	public static Location getRespawnLocation(Team t) {
		Game g = Game.getInstance();
		Location r = t.getBase(),
				l = Tools.getRandomLocation(r, 5, 15);
		boolean blocked = false;
		do {
			if(g.inArea(l)) {
				blocked = false;
				for(Schematic j : g.getJails()) {
					if(Math.abs(l.getBlockX()-j.getBlockX()) <= j.getWidth()
							&& Math.abs(l.getBlockY()-j.getBlockY()) <= j.getHeight()
							&& Math.abs(l.getBlockZ()-j.getBlockZ()) <= j.getLength()) {
						l = Tools.getRandomLocation(r, 5, 10);
						blocked = true;
						break;
					}
				}
			} else {
				l = Tools.getRandomLocation(r, 5, 10);
				blocked = true; 
			}
		} while(blocked);
		return l;
	}

}
