package de.wrenchbox.ctf.Util;

import java.io.FileInputStream;
import java.io.IOException;

import net.minecraft.server.v1_8_R1.NBTCompressedStreamTools;
import net.minecraft.server.v1_8_R1.NBTTagCompound;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.metadata.MetadataValue;

import de.wrenchbox.ctf.CaptureTheFlag;

public class Schematic {

	private Location root;
	private short width, height, length;

	@SuppressWarnings("deprecation")
	public Schematic(String filename, Location root) throws IOException {
		FileInputStream fis = new FileInputStream(CaptureTheFlag.getPlugin().getDataFolder().getPath()+"/schematics/"+filename);
		NBTTagCompound nbtdata = NBTCompressedStreamTools.a(fis);

		width = nbtdata.getShort("Width");
		height = nbtdata.getShort("Height");
		length = nbtdata.getShort("Length");

		byte[] blocks = nbtdata.getByteArray("Blocks");
		byte[] data = nbtdata.getByteArray("Data");

		this.root = root.clone().subtract(width/2, 0, length/2);
		Location l; 

		for(int y = height-1; y >= 0; y--) {
			for(int z = 0; z < length; z++) {
				for(int x = 0; x < width; x++) {

					l = this.root.clone().add(x, y, z);
					int index = x + (y * length + z) * width;

					Block b = l.getBlock();

					Meta.protect(b);
					Meta.setMetadata(b, "state", b.getState());

					b.setTypeId(blocks[index] < 0 ? Material.SPONGE.getId() : blocks[index]);
					b.setData(data[index]);
				}
			}
		}
		fis.close();
	}

	public void restore() {
		Location l;
		for(int y = 0; y < height; y++) {
			for(int z = 0; z < length; z++) {
				for(int x = 0; x < width; x++) {
					l = root.clone().add(x, y, z);

					Block b = l.getBlock();

					Meta.unprotect(b);
					MetadataValue mv = Meta.getMetadata(b, "state"); 
					if(mv != null && mv.value() instanceof BlockState) {
						((BlockState) mv.value()).update(true);
						Meta.removeMetadata(b, "state");
					}
				}
			}
		}
	}

	public int getBlockX() {
		return root.getBlockX();
	}

	public int getBlockY() {
		return root.getBlockY();
	}

	public int getBlockZ() {
		return root.getBlockZ();
	}

	public short getWidth() {
		return width;
	}

	public short getHeight() {
		return height;
	}

	public short getLength() {
		return length;
	}

}
