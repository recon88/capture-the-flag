package de.wrenchbox.ctf.Util;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class BackupManager {

	private String path;

	public BackupManager(String path) {
		this.path = path;
	}

	public void save(Player p) throws IOException {
		YamlConfiguration c = new YamlConfiguration();
		c.set("current_name", p.getName());
		c.set("food", p.getFoodLevel());
		c.set("gamemode", p.getGameMode().toString());
		c.set("health", p.getFoodLevel());
		c.set("experience", p.getTotalExperience());
		c.set("location.world", p.getLocation().getWorld().getName());
		c.set("location.x", p.getLocation().getX());
		c.set("location.y", p.getLocation().getY());
		c.set("location.z", p.getLocation().getZ());
		c.set("inventory.armor", p.getInventory().getArmorContents());
		c.set("inventory.content", p.getInventory().getContents());
		c.save(new File(path, p.getUniqueId().toString()+".yml"));
	}

	@SuppressWarnings("unchecked")
	public void restore(Player p) throws IOException {
		YamlConfiguration c = YamlConfiguration.loadConfiguration(new File(path, p.getUniqueId().toString()+".yml"));
		if(c.getString("location.world") != null) {
			p.teleport(
					new Location(
							Bukkit.getWorld(c.getString("location.world")),
							c.getDouble("location.x"),
							c.getDouble("location.y"),
							c.getDouble("location.z")
							)
					);
			ItemStack[] content = ((List<ItemStack>) c.get("inventory.armor")).toArray(new ItemStack[0]);
			p.getInventory().setArmorContents(content);
			content = ((List<ItemStack>) c.get("inventory.content")).toArray(new ItemStack[0]);
			p.getInventory().setContents(content);
			p.setFoodLevel(c.getInt("food"));
			p.setGameMode(GameMode.valueOf(c.getString("gamemode")));
			p.setHealth(c.getInt("health"));
			p.setTotalExperience(0);
			p.setLevel(0);
			p.setExp(0);
			p.giveExp(c.getInt("experience"));
		} else {
			p.teleport(Bukkit.getWorlds().get(0).getSpawnLocation());
			p.setGameMode(Bukkit.getServer().getDefaultGameMode());
		}
	}

}
