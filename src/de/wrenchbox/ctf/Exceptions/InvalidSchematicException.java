package de.wrenchbox.ctf.Exceptions;

@SuppressWarnings("serial")
public class InvalidSchematicException extends Exception {

	public InvalidSchematicException(String message) {
		super(message);
	}
}
