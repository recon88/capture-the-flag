package de.wrenchbox.ctf.Commands;

import java.util.Arrays;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.wrenchbox.ctf.Util.Messages;
import de.wrenchbox.ctf.CaptureTheFlag;
import de.wrenchbox.ctf.Game;
import de.wrenchbox.ctf.Voting;

public class VoteExecutor implements CommandExecutor {

	private Voting voting = new Voting();

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		if(!(sender instanceof Player) || Game.getInstance().playsCTF((Player) sender)) {
			if(commandLabel.equals("yes") || commandLabel.equals("no")) {
				if(voting.isEnded() || !(sender instanceof Player)) {
					return false;
				}
				Player p = (Player) sender;
				if(voting.vote(p, commandLabel.equals("yes"))) {
					Messages.put("{choice}", commandLabel);
					Messages.sendPlayer("vote-choice", p);
				} else {
					Messages.sendPlayer("one-vote-only", p);
				}
			} else if(voting.isEnded()) {
				if(args.length >= 2) {
					if(args[0].equals("kick")) {
						return handleKick(sender, commandLabel, args);
					} else {
						return false;
					}
				} else if(args.length == 1) {
					if(args[0].equals("restart")) {
						return handleRestart(sender, commandLabel, args);
					} else {
						return false;
					}
				} else {
					return false;
				}
			} else {
				Messages.sendPlayer("vote-running", sender);
			}
		} else {
			Messages.sendPlayer("not-participating", sender);
		}
		return true;
	}

	private boolean handleRestart(CommandSender sender, String commandLabel, String[] args) {
		String who;
		if(sender instanceof Player) {
			Player q = (Player) sender;
			who = q.getDisplayName();
		} else {
			who = ChatColor.DARK_PURPLE+"Server admin";
		}
		Messages.put("{player}", who);
		Messages.sendBroadcast("restart-vote");
		voting.issue(new Runnable() {

			@Override
			public void run() {
				evaluateRestartVote();
			}
		});
		Messages.put("{time}", String.valueOf(CaptureTheFlag.getPlugin().getConfig().getInt("settings.vote")));
		Messages.sendBroadcast("vote-call");
		return true;
	}

	private boolean handleKick(CommandSender sender, String commandLabel, String[] args) {
		@SuppressWarnings("deprecation")
		final Player p = Bukkit.getPlayer(args[1]);
		if(p != null) {
			if(args.length > 2) {
				final String reason = StringUtils.join(Arrays.copyOfRange(args, 2, args.length), " ");
				String who;
				if(sender instanceof Player) {
					Player q = (Player) sender;
					who = q.getDisplayName();
				} else {
					who = ChatColor.DARK_PURPLE+"Server admin";
				}
				Messages.put("{player}", who);
				Messages.put("{target}", p.getDisplayName());
				Messages.put("{reason}", reason);
				Messages.sendBroadcast("votekick-reason");
				voting.issue(new Runnable() {

					@Override
					public void run() {
						evaluateKickVote(p, reason);
					}
				});
			} else {
				String who;
				if(sender instanceof Player) {
					Player q = (Player) sender;
					who = q.getDisplayName();
				} else {
					who = ChatColor.DARK_PURPLE+"Server admin";
				}
				Messages.put("{player}", who);
				Messages.put("{target}", p.getDisplayName());
				Messages.sendBroadcast("votekick");
				voting.issue(new Runnable() {

					@Override
					public void run() {
						evaluateKickVote(p);
					}
				});
			}
			Messages.put("{time}", String.valueOf(CaptureTheFlag.getPlugin().getConfig().getInt("settings.vote")));
			Messages.sendBroadcast("vote-call");
		} else {
			return false;
		}
		if(sender instanceof Player) {
			voting.vote((Player) sender, true);
		}
		return true;
	}

	private void evaluateKickVote(Player p, String reason) {
		if(voting.isAccepted()) {
			p.kickPlayer(Messages.get("kicked") + ": " + reason);
			Messages.put("{reason}", reason);
			Messages.put("{target}", p.getDisplayName());
			Messages.sendBroadcast("player-kicked-reason");
		} else {
			Messages.put("{reason}", reason);
			Messages.put("{target}", p.getDisplayName());
			Messages.sendBroadcast("votekick-reason-failed");
		}
	}

	private void evaluateKickVote(Player p) {
		if(voting.isAccepted()) {
			p.kickPlayer(Messages.get("kicked"));
			Messages.put("{target}", p.getDisplayName());
			Messages.sendBroadcast("player-kicked");
		} else {
			Messages.put("{target}", p.getDisplayName());
			Messages.sendBroadcast("votekick-failed");
		}
	}

	private void evaluateRestartVote() {
		if(voting.isAccepted()) {
			Bukkit.dispatchCommand(Bukkit.getServer().getConsoleSender(), "reset");
		} else {
			Messages.sendBroadcast("restart-vote-failed");
		}	
	}
}
