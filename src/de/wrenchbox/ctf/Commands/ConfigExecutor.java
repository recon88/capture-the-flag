package de.wrenchbox.ctf.Commands;

import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import de.wrenchbox.ctf.CaptureTheFlag;
import de.wrenchbox.ctf.Game;

public class ConfigExecutor implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {

		if(commandLabel.equalsIgnoreCase("config") || commandLabel.equalsIgnoreCase("cfg")) {
			if(args.length == 2) {
				FileConfiguration c = CaptureTheFlag.getPlugin().getConfig();
				if(c.contains("settings."+args[0])) {
					Object o = c.get("settings."+args[0]);
					try {
						if(o instanceof Double) {
							c.set("settings."+args[0], Double.parseDouble(args[1]));
						} else if(o instanceof Integer) {
							c.set("settings."+args[0], Integer.parseInt(args[1]));
						} else if(o instanceof Boolean) {
							c.set("settings."+args[0], Boolean.parseBoolean(args[1]));
						} else {
							c.set("settings."+args[0], String.valueOf(args[1]));
						}
						sender.sendMessage("Successfully changed '"+args[0]+"' to '"+String.valueOf(c.get("settings."+args[0]))+"'.");
						CaptureTheFlag.getPlugin().saveConfig();
						if(args[0].equals("gamemode")) {
							for(Player p :Game.getInstance().getPlayers()) {
								p.setGameMode(GameMode.valueOf(args[1]));
							}
						}
					} catch(NumberFormatException e) {
						sender.sendMessage("Bad format: "+e.getMessage());
						sender.sendMessage("Input has to look like: "+String.valueOf(c.get("settings."+args[0])));
					} catch(IllegalArgumentException e) {
						sender.sendMessage("Game mode can either be CREATIVE, ADVENTURE or SURVIVAL");
					}
				} else {
					sender.sendMessage("'"+args[0]+"' does not exists.");
				}
			} else {
				return false;
			}
		}
		return true;
	}
}
