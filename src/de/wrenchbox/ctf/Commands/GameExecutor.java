package de.wrenchbox.ctf.Commands;

import java.util.logging.Level;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.wrenchbox.ctf.Util.Messages;
import de.wrenchbox.ctf.Util.Meta;
import de.wrenchbox.ctf.Util.Performance;
import de.wrenchbox.ctf.CaptureTheFlag;
import de.wrenchbox.ctf.Game;
import de.wrenchbox.ctf.Score;
import de.wrenchbox.ctf.Team;

public class GameExecutor implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {

		if(commandLabel.equalsIgnoreCase("ctf")) {
			if(sender instanceof Player) {
				if(args.length >= 1) {
					Player player = (Player) sender;
					if(args[0].equalsIgnoreCase("join")) {
						if(!Game.getInstance().playsCTF(player)) {
							Game.getInstance().joinCTF(player);
						}
					} else if(args[0].equalsIgnoreCase("leave")) {
						if(Game.getInstance().playsCTF(player)) {
							Game.getInstance().leaveCTF(player);
						}
					}
				} else {
					return false;
				}
			}
		} else if(!(sender instanceof Player) || Game.getInstance().playsCTF((Player) sender)) {
			if(commandLabel.equalsIgnoreCase("reset")) {
				if(Game.getInstance() == null)
					return false;
				Game.getInstance().reset();
			} else if(commandLabel.equalsIgnoreCase("score")) {
				String[] msg = new String[Game.getInstance().totalPlayers()+2];
				int i = 0;
				msg[i++] = String.format("%s | %s | %s", "Points", "Kills", "Deaths");
				msg[i++] = "----------------------------------------";
				for(Player p : Game.getInstance().getPlayers()) {
					Score s = Score.get(p);
					if(s != null) {
						msg[i++] = String.format("%7d | %5d | %6d  %s", s.getPoints(), s.getKills(), s.getDeaths(), p.getDisplayName());
					}
				}
				if(sender instanceof Player) {
					Player player = (Player) sender;
					player.sendMessage(msg);
				} else {
					Bukkit.getLogger().log(Level.ALL, "Scoreboard", msg);
				}
			} else if(commandLabel.equalsIgnoreCase("performance")) {
				Performance test = Performance.getInstance("performance");	
				test.startTest();
				StringBuilder msg = new StringBuilder("Performance");
				for(String key : Performance.keySet()) {
					Performance p = Performance.getInstance(key);
					long sum = p.getSum();
					msg.append(String.format("\n"+ChatColor.GREEN+"%7.2fms (%2d%%); "+ChatColor.BLUE+"avg: %6.2fms; "+ChatColor.DARK_PURPLE+"recent: %.2f; "+ChatColor.RED+"%s", sum/1000000.0, 100*sum/Performance.getTotal(), p.getAvg()/1000000.0, 10*p.getRecent()/sum, key));
				}
				sender.sendMessage(msg.toString());
				test.stopTest();
			} else if(sender instanceof Player) {
				if(Game.getInstance().playsCTF((Player) sender)) {
					if(commandLabel.equalsIgnoreCase("all")) {
						if (args.length == 0)
							return false;

						Player player = (Player) sender;

						String msg = StringUtils.join(args, " ");
						Bukkit.broadcastMessage("[All] <" + player.getDisplayName() + "> " + msg);
					} else if(commandLabel.equalsIgnoreCase("team")) {
						if(!(sender instanceof Player))
							return false;
						if(!Game.getInstance().isRunning())
							return false;

						Player player = (Player) sender;
						Team t = Team.getTeamByPlayer(player);

						if(Meta.isAncient(player)) {
							Messages.sendPlayer("change-with-flag", player);
						} else if(Game.getInstance().isCTF() && Meta.isAlive(player) && t.getBase().distanceSquared(player.getLocation()) > Math.pow(CaptureTheFlag.getPlugin().getConfig().getInt("settings.change-team-range"), 2)) {
							Messages.sendPlayer("change-range", player);
						} else {
							Team.getTeam(0).add(player);
							player.teleport(Game.getInstance().getSpawn());
						}
					}
				}
			}
		} else {
			Messages.sendPlayer("not-participating", sender);
		}
		return true;
	}
}
