package de.wrenchbox.ctf;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.UUID;

import lib.PatPeter.SQLibrary.Database;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import de.wrenchbox.ctf.Util.Performance;
import de.wrenchbox.ctf.Util.SQL;

public class Score {

	private int kills = 0,
			deaths = 0,
			points = 0,
			money = 0;

	private static HashMap<UUID, Score> scoreboard = new HashMap<UUID, Score>();

	public Score() {}

	public Score(int kills, int deaths, int points) {
		this.kills = kills;
		this.deaths = deaths;
		this.points = points;
	}

	public static Score get(Player p) {
		return scoreboard.get(p.getUniqueId());
	}

	public static void add(Player p) {
		if(!scoreboard.containsKey(p.getUniqueId()))
			scoreboard.put(p.getUniqueId(), new Score());
	}

	public int getKills() {
		return kills;
	}

	public int getDeaths() {
		return deaths;
	}

	public int getPoints() {
		return points;
	}

	public void incKills(int earn) {
		kills++;
		money += earn;  
	}

	public void incDeaths() {
		deaths++;
	}

	public void incPoints(int earn) {
		points++;
		money += earn;
	}

	public boolean pay(int price) {
		if(price <= money) {
			money -= price;
		}
		return false;
	}

	public static void save() {
		Performance test = Performance.getInstance("saveScore");
		try {
			test.startTest();
			for(UUID id : scoreboard.keySet()) {
				Player player = Bukkit.getPlayer(id);
				if(player != null) {
					save(player);
				}
			}
			long id = Game.getInstance().getId();
			if(id > 0) {
				Database db = SQL.createDatabase();
				if(db.open()) {
					String prefix = CaptureTheFlag.getPlugin().getConfig().getString("database.prefix");
					int i = 0;
					PreparedStatement ps = db.prepare("UPDATE "+prefix +"_game SET end = ? WHERE id = ?");
					ps.setTimestamp(++i, new Timestamp(System.currentTimeMillis()));
					ps.setLong(++i, id);
					db.query(ps);

					db.close();
				}
			}
			scoreboard.clear();
		} catch (SQLException e) {
			Bukkit.broadcastMessage(ChatColor.DARK_PURPLE+"Error while accessing database. Scores cannot be saved: "+e.getMessage());
		} finally {
			test.stopTest();
		}
	}

	public static void save(Player player) throws SQLException {
		Database db = SQL.createDatabase();
		if(db.open()) {
			Game g = Game.getInstance();
			String prefix = CaptureTheFlag.getPlugin().getConfig().getString("database.prefix");
			int i = 0;
			// FIXME uuid
			PreparedStatement ps = db.prepare("SELECT id FROM "+prefix +"_player WHERE uuid = ?");
			ps.setString(++i, player.getUniqueId().toString());
			ResultSet rs = db.query(ps);

			i = 0;
			if(rs.next()) {
				ps = db.prepare("UPDATE "+prefix +"_player SET lastSeen = ? WHERE id = ?");
				ps.setTimestamp(++i, new Timestamp(System.currentTimeMillis()));
				ps.setInt(++i, rs.getInt(1));
			} else {
				ps.close();
				ps = db.prepare("INSERT INTO "+prefix +"_player (name, lastSeen) VALUES (?, ?)");
				ps.setString(++i, player.getName());
				ps.setTimestamp(++i, new Timestamp(System.currentTimeMillis()));
			}
			db.query(ps);

			int tid = Team.getTeamByPlayer(player).ordinal();
			if(tid != 0) {
				Score score = get(player);
				i = 0;
				ps = db.prepare("INSERT INTO "+prefix +"_game_player (player, team, game, kills, deaths, points, score) SELECT id, ?, ?, ?, ?, ?, ? FROM "+prefix+"_player WHERE name = ?");
				ps.setInt(++i, (tid-1)%2);
				ps.setLong(++i, g.getId());
				ps.setInt(++i, score.getKills());
				ps.setInt(++i, score.getDeaths());
				ps.setInt(++i, score.getPoints());
				ps.setInt(++i, (int) player.getTotalExperience());
				ps.setString(++i, player.getName());
				db.query(ps);

				player.setLevel(0);
				player.setExp(0);
				player.setTotalExperience(0);
			}
			scoreboard.remove(player.getName());
			db.close();
		}
	}

	public static void load(Player player, Team team) throws SQLException {
		Database db = SQL.createDatabase();
		if(db.open()) {
			int tid = team.ordinal();
			if(tid != 0) {
				Game g = Game.getInstance();
				String prefix = CaptureTheFlag.getPlugin().getConfig().getString("database.prefix");
				int i = 0;
				// FIXME uuid
				PreparedStatement ps = db.prepare("SELECT g.id, kills, deaths, points, score FROM "+prefix+"_game_player AS g JOIN "+prefix+"_player AS p ON player = p.id WHERE uuid = ? AND game = ? AND team = ?");
				ps.setString(++i, player.getUniqueId().toString());
				ps.setLong(++i, g.getId());
				ps.setInt(++i, (tid-1)%2);
				ResultSet rs = db.query(ps);

				if(rs.next()) {
					scoreboard.put(player.getUniqueId(), new Score(rs.getInt("kills"), rs.getInt("deaths"), rs.getInt("points")));
					player.setLevel(0);
					player.setExp(0);
					player.setTotalExperience(0);
					player.giveExp(rs.getInt("score"));
					i = 0;
					ps = db.prepare("DELETE FROM "+prefix+"_game_player WHERE id = ?");
					ps.setInt(++i, rs.getInt("id"));
					db.query(ps);
				} else {
					add(player);
				}
			}
			db.close();
		}
	}
}
