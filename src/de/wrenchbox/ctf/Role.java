package de.wrenchbox.ctf;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

public enum Role {
	FIGHTER,
	ARCHER,
	PIONEER,
	SPY;

	Role role;

	private Role() {
		role = this;
	}

	public String toString() {
		Role r = Role.values()[role.ordinal()];
		return r.name().substring(0, 1)+r.name().substring(1).toLowerCase();
	}

	public void createInventory(Player player) {
		ItemStack[] armorSet = new ItemStack[4];
		PlayerInventory inventory = player.getInventory();
		// Clear inventory on role/team change
		inventory.clear();
		Team t = Team.getTeamByPlayer(player);
		switch (this) {
		case FIGHTER:
			armorSet[0] = new ItemStack(Material.IRON_BOOTS);
			armorSet[1] = new ItemStack(Material.IRON_LEGGINGS);
			armorSet[2] = new ItemStack(Material.IRON_CHESTPLATE);
			inventory.setItem(0, new ItemStack(Material.IRON_SWORD));
			inventory.setItem(1, new ItemStack(Material.WOOD_SPADE));
			break;
		case ARCHER:
			armorSet[0] = new ItemStack(Material.CHAINMAIL_BOOTS);
			armorSet[1] = new ItemStack(Material.CHAINMAIL_LEGGINGS);
			armorSet[2] = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
			inventory.setItem(0, new ItemStack(Material.WOOD_SWORD));
			ItemStack bow = new ItemStack(Material.BOW);
			bow.addEnchantment(Enchantment.ARROW_INFINITE, 1);
			inventory.setItem(1, bow);
			inventory.setItem(2, new ItemStack(Material.IRON_AXE));
			inventory.setItem(3, new ItemStack(Material.ARROW, 1));
			break;
		case PIONEER:
			armorSet[0] = t.colorizeArmor(new ItemStack(Material.LEATHER_BOOTS));
			armorSet[1] = t.colorizeArmor(new ItemStack(Material.LEATHER_LEGGINGS));
			armorSet[2] = t.colorizeArmor(new ItemStack(Material.LEATHER_CHESTPLATE));
			inventory.setItem(0, new ItemStack(Material.STONE_SWORD));
			inventory.setItem(1, new ItemStack(Material.IRON_PICKAXE));
			inventory.setItem(2, new ItemStack(Material.IRON_SPADE));
			break;
		case SPY:
			armorSet[0] = new ItemStack(Material.LEATHER_BOOTS);
			armorSet[1] = new ItemStack(Material.LEATHER_LEGGINGS);
			armorSet[2] = new ItemStack(Material.LEATHER_CHESTPLATE);
			inventory.setItem(0, new ItemStack(Material.STONE_SWORD));
			inventory.setItem(1, new ItemStack(Material.COMPASS));
			ItemStack potion = new ItemStack(Material.EXP_BOTTLE);
			ItemMeta im = potion.getItemMeta();
			im.setDisplayName("Invisibility");
			potion.setItemMeta(im);
			inventory.setItem(2, potion);
			break;
		}
		armorSet[3] = t.colorizeArmor(new ItemStack(Material.LEATHER_HELMET));
		inventory.setArmorContents(armorSet);
	}
}
