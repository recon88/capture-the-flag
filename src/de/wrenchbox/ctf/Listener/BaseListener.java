package de.wrenchbox.ctf.Listener;

import java.util.Iterator;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.BlockSpreadEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.material.Wool;
import org.bukkit.plugin.Plugin;

import de.wrenchbox.ctf.Exceptions.InvalidSchematicException;
import de.wrenchbox.ctf.Util.Messages;
import de.wrenchbox.ctf.Util.Meta;
import de.wrenchbox.ctf.CaptureTheFlag;
import de.wrenchbox.ctf.Game;
import de.wrenchbox.ctf.Team;

public class BaseListener implements Listener {

	public BaseListener(Plugin plugin) {
		Bukkit.getPluginManager().registerEvents(this, plugin);
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void	onBlockPlace(BlockPlaceEvent event) {
		if(!Game.getInstance().playsCTF(event.getPlayer())) {
			return;
		}
		Player p = event.getPlayer();
		if(Meta.isAncient(p)) {
			event.setCancelled(true);
			return;
		}

		Block b = event.getBlock();	
		if(Meta.isProtected(b)) {
			event.setCancelled(true);
			Messages.sendPlayer("protected", p);
			return;
		}
		Team t = Team.getTeamByPlayer(p);

		// Place wool block in team color
		if(b.getType() == Material.WOOL && Game.getInstance() != null && Game.getInstance().isRunning() && t != null && t.getBase() == null && t.getWoolData() == b.getData()) {

			// Border too close
			Location a = Game.getInstance().getArea();
			int distX = Math.abs(a.getBlockX()-b.getLocation().getBlockX()),
					distZ = Math.abs(a.getBlockZ()-b.getLocation().getBlockZ()),
					distMax = Game.getInstance().getDimension()/2-6;
			if(distX <= 6 || distX > distMax || distZ > distMax) {
				Messages.sendPlayer("near-border", p);
				p.updateInventory();
				event.setCancelled(true);
				return;
			}

			// found base
			try {
				t.foundBase(p, b);
			} catch (InvalidSchematicException e) {
				Messages.put("{error}", e.getMessage());
				Messages.sendPlayer("build-error", p);
				Bukkit.getLogger().log(Level.SEVERE, "Failed to build base. "+e.getMessage());
			}

			// Game is ready/both bases are built
			Game g = Game.getInstance();
			if(g.isReady()) {
				// countdount
				g.startCountdown();
				Messages.put("{time}", String.valueOf(g.getCountdown()));
				Messages.sendBroadcast("prepare for game");
			}
		}
	}

	/**
	 * This method handles interaction with the flags.
	 * 
	 * @param event The event object.
	 */
	@EventHandler(priority=EventPriority.HIGHEST,ignoreCancelled=true)
	public void onPlayerInteract(PlayerInteractEvent event) {
		if(!Game.getInstance().playsCTF(event.getPlayer())) {
			return;
		}
		Block b = event.getClickedBlock();
		if(b == null || !Meta.isFlag(b))
			return;

		Player p = event.getPlayer();
		Team t = Team.getTeamByPlayer(p),
				u = t.getEnemyTeam();

		if(Game.getInstance().isCTF()) {
			// CTF started
			long now = System.currentTimeMillis();
			if(Meta.getMetadata(b, "capture-delay") == null || Meta.getMetadata(b, "capture-delay").asLong() <= now) {
				if(((Wool) b.getState().getData()).getColor() == u.getColor()) {
					// clicked on enemy flag
					if(t.size() > 0 && u.size() > 0) { // TODO size > 1
						// take flag and mark player
						b.setType(Material.AIR);
						u.stealFlag(p);
						Messages.put("{player}", p.getDisplayName());
						Messages.put("{team}", u.colorizeText());
						Messages.sendBroadcast("flag-capture");
					} else {
						Messages.sendPlayer("capture-forbidden", p);
					}
				} else if(CaptureTheFlag.getPlugin().getConfig().getBoolean("settings.return-flag-on-click") && ((Wool) b.getState().getData()).getColor() == t.getColor()) {
					// clicked on own flag
					if(!b.equals(t.getFlag())) {
						// flag is not at base
						t.returnFlag();
						Messages.put("{team}", t.colorizeText());
						Messages.sendBroadcast("flag-returned");
					}
				}
			} else {
				Messages.put("{seconds}", String.format("%.1f", (Meta.getMetadata(b, "capture-delay").asLong()-now)/1000.0));
				Messages.sendPlayer("wait-capture-delay", p);
			}
		} else if(((Wool) b.getState().getData()).getColor() == t.getColor()) {
			// CTF is not yet started; clicked on own flag
			t.destroyBase();
		}
	}

	@EventHandler
	public void onBlockBreak(BlockBreakEvent event) {
		if(!Game.getInstance().playsCTF(event.getPlayer())) {
			return;
		}
		Block b = event.getBlock();
		Player p = event.getPlayer();

		// Cancel on block is protected
		if(Meta.isProtected(b)) {
			event.setCancelled(true);
			return;
		}
		if(Meta.isAncient(p)) {
			event.setCancelled(true);
			return;
		}
	}

	@EventHandler
	public void onBaseExplode(EntityExplodeEvent event) {
		if(!event.getLocation().getWorld().equals(Game.getInstance().getSpawn().getWorld())) {
			return;
		}
		Iterator<Block> it = event.blockList().iterator();
		while(it.hasNext())
			if(Meta.isProtected(it.next()))
				it.remove();
	}

	@EventHandler
	public void onBlockBurn(BlockBurnEvent event) {
		if(!event.getBlock().getWorld().equals(Game.getInstance().getSpawn().getWorld())) {
			return;
		}
		if(Meta.isProtected(event.getBlock()))
			event.setCancelled(true);
	}

	@EventHandler
	public void onBlockIgnite(BlockIgniteEvent event) {
		if(!event.getBlock().getWorld().equals(Game.getInstance().getSpawn().getWorld())) {
			return;
		}
		if(Meta.isProtected(event.getBlock()))
			event.setCancelled(true);
	}

	@EventHandler
	public void onBlockSpread(BlockSpreadEvent event) {
		if(!event.getBlock().getWorld().equals(Game.getInstance().getSpawn().getWorld())) {
			return;
		}
		if(Meta.isProtected(event.getBlock()))
			event.setCancelled(true);
	}

	@EventHandler
	public void onPlayerBucketEmpty(PlayerBucketEmptyEvent event) {
		if(!Game.getInstance().playsCTF(event.getPlayer())) {
			return;
		}
		if(Meta.isProtected(event.getBlockClicked()))
			event.setCancelled(true);
	}

	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent event) {
		if(!Game.getInstance().playsCTF(event.getEntity())) {
			return;
		}
		Team.dropFlag(event.getEntity());
	}

	@EventHandler
	public void onPistonExtend(BlockPistonExtendEvent event) {
		if(!event.getBlock().getWorld().equals(Game.getInstance().getSpawn().getWorld())) {
			return;
		}
		for(Block b : event.getBlocks()) {
			if(Meta.isProtected(b) || Meta.isProtected(b.getRelative(event.getDirection()))) {
				event.setCancelled(true);
				break;
			}
		}
	}

	@EventHandler
	public void onPistonRetract(BlockPistonRetractEvent event) {
		if(!event.getBlock().getWorld().equals(Game.getInstance().getSpawn().getWorld())) {
			return;
		}
		Block b = event.getRetractLocation().getBlock();
		if(Meta.isProtected(b) && event.isSticky() && !b.getRelative(event.getDirection()).isEmpty()) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onBlockFromTo(BlockFromToEvent event) {
		if(!event.getBlock().getWorld().equals(Game.getInstance().getSpawn().getWorld())) {
			return;
		}
		Block b = event.getToBlock();
		if(Meta.isProtected(b)) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onSandFall(EntityChangeBlockEvent event) {
		if(!event.getBlock().getWorld().equals(Game.getInstance().getSpawn().getWorld())) {
			return;
		}
		if(event.getEntity().getType() == EntityType.FALLING_BLOCK && Meta.isProtected(event.getBlock().getRelative(BlockFace.DOWN))) {
			event.setCancelled(true);
		}
	}
}
