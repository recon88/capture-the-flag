package de.wrenchbox.ctf.Listener;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.plugin.Plugin;

import de.wrenchbox.ctf.Util.*;
import de.wrenchbox.ctf.CaptureTheFlag;
import de.wrenchbox.ctf.Game;

public class SetupListener implements Listener {

	public SetupListener(Plugin plugin) {
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

	@EventHandler(priority=EventPriority.LOW)
	public void onPlayerJoin(PlayerJoinEvent event) {
		if(!CaptureTheFlag.getPlugin().getConfig().getBoolean("settings.force-ctf")) {
			return;
		}
		Game.getInstance().joinCTF(event.getPlayer());
	}

	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event) {
		if(!Game.getInstance().playsCTF(event.getPlayer())) {
			return;
		}
		Player player = event.getPlayer();
		Block placedblock = event.getBlock();
		Game g = Game.getInstance();
		if (g != null && !g.isRunning() && placedblock.getType().equals(Material.SPONGE)) {
			placedblock.setType(Material.AIR);
			Game.getInstance().start(placedblock.getLocation());
			player.getInventory().clear();
			player.setAllowFlight(false);
			player.teleport(Game.getInstance().getSpawn());
			Messages.sendBroadcast("game-started");
		}
	}

	@EventHandler
	public void onPlayerRespawn(PlayerRespawnEvent event) {
		if(!Game.getInstance().playsCTF(event.getPlayer())) {
			return;
		}
		final Player player = event.getPlayer();
		final Game g = Game.getInstance();
		if(!g.isRunning() && g.playersAlive() == 0) {
			g.giveStartBlock(player);
		}
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		if(!Game.getInstance().playsCTF(event.getPlayer())) {
			return;
		}
		Game.getInstance().leaveCTF(event.getPlayer());
	}

	@EventHandler
	public void onPlayerClickChest(PlayerInteractEvent event) {
		if(!Game.getInstance().playsCTF(event.getPlayer())) {
			return;
		}
		Block b = event.getClickedBlock();
		if(b == null) {
			return;
		}
		Game g = Game.getInstance();
		if(!g.isRunning() && b.getType() == Material.CHEST) {
			Player p = event.getPlayer();
			event.setCancelled(true);
			Messages.sendPlayer("wait-for-game", p);
			return;
		}
	}
}
