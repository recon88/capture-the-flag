package de.wrenchbox.ctf.Listener;

import java.io.IOException;
import java.util.logging.Level;

import net.minecraft.server.v1_8_R1.EnumClientCommand;
import net.minecraft.server.v1_8_R1.PacketPlayInClientCommand;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent.RegainReason;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerToggleSprintEvent;
import org.bukkit.event.world.PortalCreateEvent;
import org.bukkit.material.Wool;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import de.wrenchbox.ctf.Util.Messages;
import de.wrenchbox.ctf.Util.Meta;
import de.wrenchbox.ctf.Util.Performance;
import de.wrenchbox.ctf.Util.Schematic;
import de.wrenchbox.ctf.Util.Tools;
import de.wrenchbox.ctf.CaptureTheFlag;
import de.wrenchbox.ctf.Game;
import de.wrenchbox.ctf.Score;
import de.wrenchbox.ctf.Team;

public class PlayerListener implements Listener {

	public PlayerListener(Plugin p) {
		p.getServer().getPluginManager().registerEvents(this, p);
	}

	@EventHandler
	public void onPlayerMove(PlayerMoveEvent event) {
		if(!Game.getInstance().playsCTF(event.getPlayer())) {
			return;
		}
		Performance test = Performance.getInstance("move");
		test.startTest();

		Player player = event.getPlayer();

		if(Team.getTeamByPlayer(player).ordinal() == 0) {
			test.stopTest();
			return;
		}

		if(!event.getFrom().getBlock().equals(event.getTo().getBlock())) {
			if(Meta.isAlive(player)) {
				test.stopTest();
				onPlayerStep(event);
			} else {
				test.stopTest();
			}
		} else {
			test.stopTest();
		}
	}


	/**
	 * Only called when a player crosses the border between two blocks. In other words: Only called
	 * when the player's BlockX or BlockZ coordinate changes.
	 * 
	 * @param event The event object.
	 */
	public void onPlayerStep(PlayerMoveEvent event) {
		Performance test = Performance.getInstance("step");
		test.startTest();

		Player player = event.getPlayer();
		if(Game.getInstance().isCTF()) {
			for(Team t : Game.getInstance().getTeams()) {
				if(Meta.getAncient(t) != null && Meta.getAncient(t).equals(player)) {
					Block flag = t.getFlag();
					if(Meta.getMetadata(flag, "distance") == null || Meta.getMetadata(flag, "distance").asInt() == 0) {

						Location p = player.getLocation(),
								f = flag.getLocation();
						int dist = Math.abs(p.getBlockX()-f.getBlockX()),
								temp = Math.abs(p.getBlockY()-f.getBlockY());
						if(temp > dist)
							dist = temp;
						temp = Math.abs(p.getBlockZ()-f.getBlockZ());
						if(temp > dist)
							dist = temp;

						int range = 3;

						if(dist <= range && Meta.isFlag(flag)) {
							t.earnPoint(player);
						} else {
							Meta.setMetadata(t.getFlag(), "distance", dist > range ? dist-range : 1);
						}
					} else {
						Meta.setMetadata(t.getFlag(), "distance", Meta.getMetadata(t.getFlag(), "distance").asInt()-1);
					}
				}
			}
		}
		test.stopTest();
		Game.getInstance().checkPlayerLocation(event.getTo(), player);
	}

	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent event) {
		if(!Game.getInstance().playsCTF(event.getPlayer())) {
			return;
		}
		Team team = Team.getTeamByPlayer(event.getPlayer());

		event.getRecipients().clear();
		event.getRecipients().addAll(team);

		event.setFormat("[Team] <"+team.colorizeChat("%s")+"> %s");
	}

	@EventHandler
	public void onPlayerRegainHealth(EntityRegainHealthEvent event) {
		if(event.getEntity() instanceof Player && !Game.getInstance().playsCTF((Player) event.getEntity())) {
			return;
		}
		if(event.getRegainReason() == RegainReason.SATIATED || event.getRegainReason() == RegainReason.REGEN) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onPlayerRegainFood(FoodLevelChangeEvent event) {
		if(event.getEntity() instanceof Player && !Game.getInstance().playsCTF((Player) event.getEntity())) {
			return;
		}
		event.setCancelled(true);
	}

	@EventHandler
	public void onPlayerDeath(final PlayerDeathEvent event) {
		if(!Game.getInstance().playsCTF(event.getEntity())) {
			return;
		}
		Player player = event.getEntity(),
				killer = event.getEntity().getKiller();
		
		if(Game.getInstance().isCTF()) {
			Meta.setMetadata(player, "death", System.currentTimeMillis());
		}
		Score.get(player).incDeaths();

		if(killer != null && !Team.getTeamByPlayer(killer).equals(Team.getTeamByPlayer(player))) {
			Score.get(killer).incKills(player.getLevel());
			killer.giveExp(player.getLevel());
		}

		event.getDrops().clear();
		event.setDroppedExp(0);
		event.setKeepLevel(true);

		if(!Game.getInstance().isRunning() && player.getInventory().contains(Material.SPONGE)) {
			player.setAllowFlight(false);
			Game.getInstance().passStartBlock();
		}
		new BukkitRunnable() {
			
			@Override
			public void run() {
				PacketPlayInClientCommand packet = new PacketPlayInClientCommand(EnumClientCommand.PERFORM_RESPAWN);
				((CraftPlayer)event.getEntity()).getHandle().playerConnection.a(packet);
			}
		}.runTask(CaptureTheFlag.getPlugin());
	}

	@EventHandler
	public void onPlayerRespawn(PlayerRespawnEvent event) {
		if(!Game.getInstance().playsCTF(event.getPlayer())) {
			return;
		}
		final Player player = event.getPlayer();
		final Game g = Game.getInstance();
		if(g.isRunning()) {
			Team t = Team.getTeamByPlayer(player);
			Location l;

			if(t.getBase() == null) {
				// spawn in game area with a wool block to found base
				l = Tools.getRandomLocation(g.getArea().clone().subtract(Math.pow(-1, Team.indexOf(t) % 2)*g.getDimension()/4, 0, 0), 1, 5);
				player.getInventory().addItem(new Wool(t.getColor()).toItemStack(1));
			} else if(t.ordinal() == 0) {
				l = t.getBase();
			} else {
				// spawn randomly around your base
				l = Tools.getRespawnLocation(t);
			}
			while(!l.getBlock().getRelative(BlockFace.UP).isEmpty()) {
				l = l.getBlock().getRelative(BlockFace.UP).getLocation();
			}

			event.setRespawnLocation(l);
			if(g.isCTF() && t.ordinal() != 0 && Meta.getMetadata(player, "death") != null) {
				int respawnDelay = CaptureTheFlag.getPlugin().getConfig().getInt("settings.respawn");
				final int delayLeft = (int) (respawnDelay-(System.currentTimeMillis()-Meta.getMetadata(player, "death").asLong())/1000);
				if(delayLeft > 0) {
					try {
						final Schematic jail = new Schematic("jail.schematic", l);
						g.getJails().add(jail);
						Messages.put("{time}", String.valueOf(delayLeft));
						Messages.sendPlayer("respawn-time", player);
						BukkitRunnable task = new BukkitRunnable() {
							int countdown = delayLeft;

							@Override
							public void run() {
								switch(--countdown) {
								case 25:
								case 20:
								case 15:
								case 10:
									Messages.put("{time}", String.valueOf(delayLeft));
									Messages.sendPlayer("respawn-time", player);
									break;
								case 3:
								case 2:
								case 1:
									player.sendMessage(countdown+"...");
									break;
								case 0:
									Messages.sendPlayer("respawn", player);
									player.setNoDamageTicks((int) (CaptureTheFlag.getPlugin().getConfig().getDouble("settings.respawn-no-dmg")*20));
									jail.restore();
									g.getJails().remove(jail);
									this.cancel();
									break;
								}
							}
						};
						task.runTaskTimer(CaptureTheFlag.getPlugin(), 20, 20);
					} catch (IOException e) {
						Bukkit.getLogger().log(Level.SEVERE, e.getMessage());
					}
				}
			}
		} else {
			event.setRespawnLocation(g.getSpawn());
		}
	}

	@EventHandler
	public void onPlayerSprint(PlayerToggleSprintEvent event) {
		if(!Game.getInstance().playsCTF(event.getPlayer())) {
			return;
		}
		Player player = event.getPlayer();

		if(!CaptureTheFlag.getPlugin().getConfig().getBoolean("settings.allow-sprinting") && event.isSprinting()) {
			player.setFoodLevel(0);
			event.setCancelled(true);
			Messages.sendPlayer("sprint", player);
		} else if(!event.isSprinting()) {
			player.setFoodLevel(20);
		}
	}

	@EventHandler
	public void onDamage(EntityDamageEvent event) {
		Entity e = event.getEntity();
		if(!(e instanceof Player) || !Game.getInstance().playsCTF((Player) e)) {
			return;
		}
		Player p = (Player) e;
		Team t = Team.getTeamByPlayer(p);
		if(t.ordinal() == 0) {
			event.setCancelled(true);
		} else {
			Team.dropFlag(p);
		}
	}

	@EventHandler
	public void onPortal(PortalCreateEvent event) {
		if(!event.getWorld().equals(Game.getInstance().getSpawn().getWorld())) {
			return;
		}
		event.setCancelled(true);
	}
}