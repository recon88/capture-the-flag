package de.wrenchbox.ctf.Listener;

import java.util.Formatter;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.DoubleChest;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.inventory.DoubleChestInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Wool;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import de.wrenchbox.ctf.Util.Messages;
import de.wrenchbox.ctf.Util.Meta;
import de.wrenchbox.ctf.CaptureTheFlag;
import de.wrenchbox.ctf.Game;
import de.wrenchbox.ctf.Role;
import de.wrenchbox.ctf.Team;

@SuppressWarnings("deprecation")
public class RoleListener implements Listener {

	public RoleListener(Plugin p) {
		p.getServer().getPluginManager().registerEvents(this, p);
	}

	private boolean isSlotBlocked(Player player, int slot) {
		if(Team.getList().indexOf(Team.getTeamByPlayer(player)) == 0)
			return true;
		Role role = (Role) (Meta.getMetadata(player, "role") != null ? Meta.getMetadata(player, "role").value() : null);
		if(role != null) {
			switch(slot) {
			case 0:
			case 1:
			case 100:
			case 101:
			case 102:
			case 103:
				return true;
			case 2:
				if(role == Role.ARCHER || role == Role.PIONEER || role == Role.SPY)
					return true;
				break;
			case 3:
				if(role == Role.ARCHER)
					return true;
				break;
			}
		}
		return false;
	}

	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		if(event.getWhoClicked() instanceof Player && !Game.getInstance().playsCTF((Player) event.getWhoClicked())) {
			return;
		}
		final Player player = (Player) event.getWhoClicked();
		if(event.getSlotType() == SlotType.QUICKBAR) {
			if(isSlotBlocked(player, event.getSlot())) {
				event.setCancelled(true);
				return;
			}
		}
		if(event.getSlotType() == SlotType.ARMOR) {
			player.performCommand("team");
			event.setCancelled(true);
			closeInventory(player);
			return;
		}
		// If player is dead
		if(!Meta.isAlive(player)) {
			Messages.sendPlayer("change-dead", player);
			event.setCancelled(true);
			closeInventory(player);
			return;
		}

		final Game g = Game.getInstance();
		if (event.getInventory() instanceof DoubleChestInventory) {
			DoubleChest chest = (DoubleChest) event.getInventory().getHolder();

			// click on own chest
			if(chest.getLeftSide().equals(g.getChest().getLeftSide())) {
				event.setCancelled(true);
				// Select a team from team chest
				if (g.isRunning()) {
					Team selectedTeam;
					int column = event.getSlot() % 9;
					switch (column) {
					case 0:
					case 1:
					case 2:
					case 3:
						selectedTeam = g.getTeams()[0];
						break;
					case 5:
					case 6:
					case 7:
					case 8:
						selectedTeam = g.getTeams()[1];
						break;
					default:
						event.setCancelled(true);
						return;
					}
					if (selectedTeam.size() >= 2 && selectedTeam.getEnemyTeam().size() < 2) {
						event.setCancelled(true);
						Messages.put("{team}", selectedTeam.getEnemyTeam().colorizeText());
						Messages.sendPlayer("be-fair", player);
						closeInventory(player);
						return;
					}
					selectedTeam.add(player);
					Role role;
					switch (column) {
					case 0:
					case 8:
						role = Role.FIGHTER;
						break;
					case 1:
					case 7:
						role = Role.ARCHER;
						break;
					case 2:
					case 6:
						role = Role.PIONEER;
						break;
					case 3:
					case 5:
						role = Role.SPY;
						break;
					default:
						role = null;
					}
					Meta.setMetadata(player, "role", role);
					role.createInventory(player);
					if(selectedTeam.getBase() == null) {
						player.getInventory().addItem(new Wool(selectedTeam.getColor()).toItemStack(1));
						Bukkit.getScheduler().scheduleSyncDelayedTask(CaptureTheFlag.getPlugin(), new Runnable() {

							@Override
							public void run() {
								player.teleport(g.getArea().getWorld().getHighestBlockAt((g.getArea().clone().subtract(Math.pow(-1, Team.getTeamByPlayer(player).ordinal() % 2)*Game.getInstance().getDimension()/4, 0, 0))).getLocation().add(0, 1, 0));
							}
						});
					} else {
						selectedTeam.setCompassTarget(player);
						Bukkit.getScheduler().scheduleSyncDelayedTask(CaptureTheFlag.getPlugin(), new Runnable() {

							@Override
							public void run() {
								player.teleport(Team.getTeamByPlayer(player).getSpawn());
							}
						});
					}

					// hide sneaking spys
					for(Player p : selectedTeam.getEnemyTeam()) {
						if(Meta.isInvisible(p)) {
							player.hidePlayer(p);
						}
					}
					// show spys on team change
					Meta.show(player);
					for(Player p : selectedTeam) {
						player.showPlayer(p);
					}

					Messages.put("{player}", player.getDisplayName());
					Messages.put("{role}", ((Role) Meta.getMetadata(player, "role").value()).toString());
					Messages.put("{team}", selectedTeam.colorizeText());
					Messages.sendTeam("player-teamjoin", selectedTeam);

					Messages.put("{player}", player.getDisplayName());
					Messages.put("{team}", selectedTeam.colorizeText());
					Messages.sendTeam("player-teamjoin-enemy", selectedTeam.getEnemyTeam());

					Messages.put("{player}", player.getDisplayName());
					Messages.put("{team}", selectedTeam.colorizeText());
					Messages.sendTeam("player-teamjoin-enemy", Team.getTeam(0));
				}
			}
		}
	}

	private void closeInventory(final Player player) {
		Bukkit.getScheduler().scheduleSyncDelayedTask(CaptureTheFlag.getPlugin(), new Runnable() {

			@Override
			public void run() {
				player.closeInventory();
			}
		});
	}

	private void updateInventory(final Player player) {
		Bukkit.getScheduler().scheduleSyncDelayedTask(CaptureTheFlag.getPlugin(), new Runnable() {

			@Override
			public void run() {;
			player.updateInventory();
			}
		});
	}

	@EventHandler(priority=EventPriority.LOWEST)
	public void onPlayerRespawn(PlayerRespawnEvent event) {
		if(!Game.getInstance().playsCTF(event.getPlayer())) {
			return;
		}
		Player player = event.getPlayer();
		if(Team.getTeamByPlayer(player).ordinal() != 0) {
			Team.getTeamByPlayer(player).setCompassTarget(player);
			Role role = (Role) Meta.getMetadata(player, "role").value();
			role.createInventory(player);
		}
	}

	@EventHandler
	public void onToogleSneak(PlayerToggleSneakEvent event) {
		if(!Game.getInstance().playsCTF(event.getPlayer())) {
			return;
		}
		final Player player = event.getPlayer();
		Team t = Team.getTeamByPlayer(player);
		if(Team.getList().indexOf(t) != 0) {
			Role r = (Role) Meta.getMetadata(player, "role").value(); 
			if(!player.isDead() && r.equals(Role.SPY)) {
				if(player.isSneaking()) {
					if(Meta.isInvisible(player)) {
						player.getWorld().playEffect(player.getLocation(), Effect.ENDER_SIGNAL, 0);
						player.getWorld().playSound(player.getLocation(), Sound.ENDERMAN_TELEPORT, 1, 1);

						long cd = Math.min(System.currentTimeMillis()-Meta.getMetadata(player, "ability").asLong()+1000, (long) (CaptureTheFlag.getPlugin().getConfig().getDouble("settings.cooldown.invisible")*1000)),
								delay = (20*cd/1000)%20;
						player.getInventory().getItem(2).setType(Material.GLASS_BOTTLE);
						player.getInventory().getItem(2).setAmount((int) Math.ceil(cd/1000)+(delay == 0 ? 0 : 1));

						new BukkitRunnable() {

							@Override
							public void run() {
								if(!player.isDead()) {
									int cd = (int) Math.ceil(Meta.getCooldown(player, "invisible")/1000);
									if(cd <= 0) {
										player.getInventory().getItem(2).setType(Material.EXP_BOTTLE);
										player.getInventory().getItem(2).setAmount(1);
										cancel();
									} else {
										player.getInventory().getItem(2).setAmount(cd);
									}
								} else {
									cancel();
								}
							}
						}.runTaskTimer(CaptureTheFlag.getPlugin(), delay == 0 ? 20 : delay, 20);

						Meta.setCooldown(player, "invisible", cd);
						Meta.show(player);
						ItemStack[] armor = (ItemStack[]) Meta.getMetadata(player, "armor").value();
						player.getInventory().setArmorContents(armor);
						player.removePotionEffect(PotionEffectType.INVISIBILITY);
					}
				} else if(Meta.onCooldown(player, "invisible")) {
					Messages.put("{seconds}", String.valueOf(new Formatter().format("%.1f", Meta.getCooldown(player, "invisible")/1000.0)));
					Messages.sendPlayer("cooldown", player);
				} else if(!Meta.isAncient(player)) {
					Meta.setMetadata(player, "ability", System.currentTimeMillis());

					player.getWorld().playEffect(player.getLocation(), Effect.ENDER_SIGNAL, 0);
					player.getWorld().playSound(player.getLocation(), Sound.ENDERMAN_TELEPORT, 1, 1);

					player.getInventory().getItem(2).setType(Material.POTION);

					Meta.hide(player);
					Meta.setMetadata(player, "armor", player.getInventory().getArmorContents());
					player.getInventory().setArmorContents(null);
					player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 1, true), true);
				}
			}
		}
	}

	@EventHandler
	public void onPlayerDropItem(PlayerDropItemEvent event) {
		if(!Game.getInstance().playsCTF(event.getPlayer())) {
			return;
		}
		Player player = event.getPlayer();
		if(isSlotBlocked(player, player.getInventory().getHeldItemSlot())) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onArmorDamage(EntityDamageEvent event) {
		if(event.getEntity() instanceof Player && !Game.getInstance().playsCTF((Player) event.getEntity())) {
			return;
		}
		if(event.getEntity().getType() == EntityType.PLAYER) {
			Player player = (Player) event.getEntity();
			for(ItemStack is : player.getInventory().getArmorContents()) {
				if(is.getType() != Material.WOOL) {
					is.setDurability((short) -10);
					updateInventory(player);	
				}
			}
		}
	}

	@EventHandler
	public void onWeaponUse(EntityDamageByEntityEvent event) {
		if(event.getEntity() instanceof Player && !Game.getInstance().playsCTF((Player) event.getEntity())) {
			return;
		}
		if(event.getDamager().getType() == EntityType.PLAYER) {
			Player player = (Player) event.getDamager();
			if(isSlotBlocked(player, player.getInventory().getHeldItemSlot())) {
				player.getItemInHand().setDurability((short) -10);
				updateInventory(player);
			}
		}
	}

	@EventHandler
	public void onToolUse(BlockBreakEvent event) {
		if(!Game.getInstance().playsCTF(event.getPlayer())) {
			return;
		}
		Player player = event.getPlayer();
		if(isSlotBlocked(player, player.getInventory().getHeldItemSlot())) {
			player.getItemInHand().setDurability((short) -10);
			updateInventory(player);
		}
	}

	@EventHandler
	public void onBowUse(EntityShootBowEvent event) {
		if(event.getEntity() instanceof Player && !Game.getInstance().playsCTF((Player) event.getEntity())) {
			return;
		}
		if(event.getEntity().getType() == EntityType.PLAYER) {
			Player player = (Player) event.getEntity();
			if(isSlotBlocked(player, player.getInventory().getHeldItemSlot())) {
				player.getItemInHand().setDurability((short) -10);
				updateInventory(player);
			}
		}
	}

	@EventHandler
	public void onDamage(EntityDamageByEntityEvent event) {
		if(event.getEntity() instanceof Player && !Game.getInstance().playsCTF((Player) event.getEntity())) {
			return;
		}
		if(event.getDamager() instanceof Player) {
			Player p = (Player) event.getDamager();
			if(Meta.isInvisible(p)) {
				event.setCancelled(true);
				return;
			}
		}
	}

	@EventHandler
	public void onProjectileLaunch(ProjectileLaunchEvent event) {
		if(event.getEntity().getShooter() instanceof Player && !Game.getInstance().playsCTF((Player) event.getEntity().getShooter())) {
			return;
		}
		Projectile projectile = event.getEntity();
		if(projectile.getShooter() instanceof Player) {
			Player p = (Player) projectile.getShooter();
			if(Meta.isInvisible(p)) {
				event.setCancelled(true);
				return;
			}
		}
	}

	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event) {
		if(!Game.getInstance().playsCTF(event.getPlayer())) {
			return;
		}
		if(Meta.isInvisible(event.getPlayer())) {
			event.setCancelled(true);
			return;
		}
	}

	@EventHandler
	public void onBlockBreak(BlockBreakEvent event) {
		if(!Game.getInstance().playsCTF(event.getPlayer())) {
			return;
		}
		if(Meta.isInvisible(event.getPlayer())) {
			event.setCancelled(true);
			return;
		}
	}

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		if(!Game.getInstance().playsCTF(event.getPlayer())) {
			return;
		}
		Player player = event.getPlayer();
		if(Meta.isInvisible(player)) {
			event.setCancelled(true);
			return;
		}
		if(player.getInventory().getHeldItemSlot() == 2 && Meta.getMetadata(player, "role") != null && ((Role) Meta.getMetadata(player, "role").value()).equals(Role.SPY)) {
			event.setCancelled(true);
			updateInventory(player);
			return;
		}
	}
}
