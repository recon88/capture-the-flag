package de.wrenchbox.ctf.Listener;

import java.util.HashMap;
import java.util.HashSet;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import de.wrenchbox.ctf.CaptureTheFlag;
import de.wrenchbox.ctf.Game;

public class WallListener implements Listener {

	private HashMap<String, HashSet<Location>> buffer = new HashMap<String, HashSet<Location>>();

	public WallListener() {
		CaptureTheFlag.getPlugin().getServer().getPluginManager().registerEvents(this, CaptureTheFlag.getPlugin());
	}

	public void save(String playerName, Location s) {
		if(!buffer.containsKey(playerName)) {
			buffer.put(playerName, new HashSet<Location>());
		}

		buffer.get(playerName).add(s);
	}

	public HashSet<Location> restore(String identifier) {
		if(buffer.containsKey(identifier)) {
			return buffer.get(identifier);
		}
		return new HashSet<Location>();
	}

	public void remove(String playerName, Location r) {
		buffer.get(playerName).remove(r);
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onWallBreak(BlockBreakEvent event) {
		if(!Game.getInstance().playsCTF(event.getPlayer())) {
			return;
		}
		Player p = event.getPlayer();
		Location l = event.getBlock().getLocation();
		if(restore(p.getName()+BlockFace.NORTH.toString()).contains(l)
				|| restore(p.getName()+BlockFace.EAST.toString()).contains(l)) {
			p.sendBlockChange(l, Material.THIN_GLASS, (byte) 0);
		}
	}
}
