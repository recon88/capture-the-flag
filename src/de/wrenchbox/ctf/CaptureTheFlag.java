package de.wrenchbox.ctf;

import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import lib.PatPeter.SQLibrary.DBMS;
import lib.PatPeter.SQLibrary.Database;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;
import org.bukkit.block.Chest;
import org.bukkit.command.CommandExecutor;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import de.wrenchbox.ctf.Commands.ConfigExecutor;
import de.wrenchbox.ctf.Commands.GameExecutor;
import de.wrenchbox.ctf.Commands.VoteExecutor;
import de.wrenchbox.ctf.Config.ConfigHandler;
import de.wrenchbox.ctf.Listener.BaseListener;
import de.wrenchbox.ctf.Listener.PlayerListener;
import de.wrenchbox.ctf.Listener.RoleListener;
import de.wrenchbox.ctf.Listener.SetupListener;
import de.wrenchbox.ctf.Util.Performance;
import de.wrenchbox.ctf.Util.SQL;

@SuppressWarnings("unused")
public class CaptureTheFlag extends JavaPlugin {

	private Listener bl, pl, sl, rl;
	private ConfigHandler ch;
	private CommandExecutor ve, ge, ce;
	private static Plugin plugin;

	public static Plugin getPlugin() {
		return plugin;
	}

	@Override
	public void onEnable() {
		plugin = this;

		try {
			Metrics metrics = new Metrics(this);
			metrics.start();
		} catch (IOException e) {
			getLogger().info("Unable to connect to metrics server.");
		}

		String[] files = {
				"jail.schematic",
				"lobby.schematic",
				"base1.schematic",
				"base2.schematic"
		};
		for(String f : files) {
			if(!new File(getDataFolder()+"/schematics/"+f).exists()) {
				saveResource("schematics/"+f, false);
			}
		}

		ch = new ConfigHandler(this);

		try {
			Database db = SQL.createDatabase();

			if(db.open()) {
				String prefix = getConfig().getString("database.prefix");
				PreparedStatement ps = db.prepare("CREATE TABLE IF NOT EXISTS `"+prefix+"_game` (" +
						"  `id` INTEGER "+(db.getDBMS() == DBMS.MySQL ? "AUTO_INCREMENT " : "")+"PRIMARY KEY," +
						"  `start` DATETIME," +
						"  `end` DATETIME" +
						");");
				db.query(ps);
				ps = db.prepare("CREATE TABLE IF NOT EXISTS `"+prefix+"_game_player` (" +
						"  `id` INTEGER "+(db.getDBMS() == DBMS.MySQL ? "AUTO_INCREMENT " : "")+"PRIMARY KEY," +
						"  `player` INTEGER," +
						"  `team` TINYINT(1)," +
						"  `game` INTEGER," +
						"  `kills` INTEGER," +
						"  `deaths` INTEGER," +
						"  `points` INTEGER," +
						"  `score` INTEGER" +
						");");
				db.query(ps);
				ps = db.prepare("CREATE TABLE IF NOT EXISTS `"+prefix+"_player` (" +
						"  `id` INTEGER "+(db.getDBMS() == DBMS.MySQL ? "AUTO_INCREMENT " : "")+"PRIMARY KEY," +
						"  `uuid` VARCHAR(36) UNIQUE," +
						"  `name` VARCHAR(16)," +
						"  `lastSeen` DATETIME" +
						");");
				db.query(ps);
				db.close();
			}
		} catch (SQLException e) {
			Bukkit.getLogger().severe("Error while accessing database. Score cannot be saved: "+e.getMessage());
		}

		long seed = CaptureTheFlag.getPlugin().getConfig().getLong("settings.seed", 0);
		World w = getServer().getWorld(CaptureTheFlag.getPlugin().getConfig().getString("settings.world"));
		if(w == null) {
			WorldCreator wc = new WorldCreator(CaptureTheFlag.getPlugin().getConfig().getString("settings.world")).type(WorldType.NORMAL);
			if(seed != 0) {
				wc.seed(seed);
			}
			w = Bukkit.createWorld(wc);
		}

		new Team(DyeColor.WHITE);
		new Game(w, new Team(DyeColor.BLUE), new Team(DyeColor.RED));

		bl = new BaseListener(this);
		pl = new PlayerListener(this);
		sl = new SetupListener(this);
		rl = new RoleListener(this);
		ve = new VoteExecutor();
		ge = new GameExecutor();
		ce = new ConfigExecutor();

		getCommand("vote").setExecutor(ve);
		getCommand("yes").setExecutor(ve);
		getCommand("no").setExecutor(ve);

		getCommand("all").setExecutor(ge);
		getCommand("reset").setExecutor(ge);
		getCommand("score").setExecutor(ge);
		getCommand("team").setExecutor(ge);
		getCommand("ctf").setExecutor(ge);
		getCommand("performance").setExecutor(ge);

		getCommand("config").setExecutor(ce);
		getCommand("cfg").setExecutor(ce);
	}

	@Override
	public void onDisable() {
		Game g = Game.getInstance();
		g.getChest().getInventory().clear();
		Chest left = (Chest) g.getChest().getLeftSide();
		Chest right = (Chest) g.getChest().getRightSide();
		left.setType(Material.AIR);
		right.setType(Material.AIR);
		left.update(true);
		right.update(true);
		g.stop();
		Performance.stopAll();
		HandlerList.unregisterAll();
	}
}
