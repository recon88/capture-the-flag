package de.wrenchbox.ctf;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class Voting {

	private HashMap<UUID, Boolean> votes = new HashMap<UUID, Boolean>();
	private boolean ended = true;
	private int taskId;
	private Runnable task;

	public void issue(Runnable task) {
		ended = false;
		this.task = task;
		taskId = Bukkit.getScheduler().scheduleSyncDelayedTask(CaptureTheFlag.getPlugin(), task, 20*CaptureTheFlag.getPlugin().getConfig().getInt("settings.vote"));
	}

	public boolean vote(Player p, boolean vote) {
		if(!votes.containsKey(p.getUniqueId())) {
			votes.put(p.getUniqueId(), vote);
			if(passed()) {
				Bukkit.getScheduler().cancelTask(taskId);
				Bukkit.getScheduler().runTask(CaptureTheFlag.getPlugin(), task);
			}
			return true;
		} else {
			return false;
		}
	}

	public boolean isAccepted() {
		ended = true;
		boolean r = passed();
		votes.clear();
		return r;
	}

	private boolean passed() {
		int yes = 0;
		for(Boolean hasVoted : votes.values()) {
			if(hasVoted) {
				yes++;
			}	
		}
		return yes > Game.getInstance().totalPlayers()/2;
	}

	public boolean isEnded() {
		return ended;
	}
}
