package de.wrenchbox.ctf;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import lib.PatPeter.SQLibrary.Database;
import net.minecraft.server.v1_8_R1.NBTCompressedStreamTools;
import net.minecraft.server.v1_8_R1.NBTTagCompound;

import org.apache.commons.lang.WordUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Chest;
import org.bukkit.block.DoubleChest;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import de.wrenchbox.ctf.Exceptions.InvalidSchematicException;
import de.wrenchbox.ctf.Listener.WallListener;
import de.wrenchbox.ctf.Util.BackupManager;
import de.wrenchbox.ctf.Util.Messages;
import de.wrenchbox.ctf.Util.Meta;
import de.wrenchbox.ctf.Util.Performance;
import de.wrenchbox.ctf.Util.SQL;
import de.wrenchbox.ctf.Util.Schematic;
import de.wrenchbox.ctf.Util.Tools;

public class Game {

	private int dimension;
	private long id;
	private Location area, spawn;
	private static Game instance;
	private Team[] teams = new Team[2];
	private boolean ctf = false;
	private int countdown;
	private BukkitTask countdownTask;
	private DoubleChest chest;
	private WallListener buffer = new WallListener();
	private Set<Schematic> jails = new HashSet<Schematic>();
	private BackupManager backup = new BackupManager(CaptureTheFlag.getPlugin().getDataFolder()+"/backup");

	public Game(World world, Team t0, Team t1) {
		this.spawn = world.getSpawnLocation();
		this.teams[0] = t0;
		this.teams[1] = t1;
		instance = this;
		Team.getTeam(0).setBase(spawn);
		for(Player p : Bukkit.getOnlinePlayers()) {
			if(p.getWorld().equals(world)) {
				Team.getTeam(0).add(p);
				Score.add(p);
			}
		}		
		passStartBlock();

		try {
			createTeamSelect(world);	
		} catch(InvalidSchematicException e) {
			Bukkit.getLogger().severe("Failed to build lobby. "+e.getMessage());
		}
	}

	@SuppressWarnings("deprecation")
	private void createTeamSelect(World w) throws InvalidSchematicException {
		spawn.setY(w.getMaxHeight() - 10);
		dimension = CaptureTheFlag.getPlugin().getConfig().getInt("settings.dimension");
		Performance test = Performance.getInstance("base");
		test.startTest();

		try {
			String baseFile = CaptureTheFlag.getPlugin().getDataFolder().getPath()+"/schematics/lobby.schematic";
			FileInputStream fis = new FileInputStream(baseFile);
			NBTTagCompound nbtdata = NBTCompressedStreamTools.a(fis);


			short width = nbtdata.getShort("Width"),
					height = nbtdata.getShort("Height"),
					length = nbtdata.getShort("Length");

			byte[] blocks = nbtdata.getByteArray("Blocks");

			int chestX = 0, chestZ = 0;
			boolean chestExists = false,
					doubleChestExists = false;

			for(int y = 0; y < height; y++) {
				for(int z = 0; z < length; z++) {
					for(int x = 0; x < width; x++) {

						int index = x + (y * length + z) * width;
						byte b = blocks[index];

						if(b == Material.CHEST.getId()) {
							if(!chestExists) {
								chestExists = true;
								chestX = x;
								chestZ = z;
							} else if(chestExists && !doubleChestExists && Math.abs(x-chestX)+Math.abs(z-chestZ) == 1) {
								doubleChestExists = true;
							} else if(doubleChestExists) {
								test.stopTest();
								throw new InvalidSchematicException("More than one double chests found in "+baseFile);
							} else {
								test.stopTest();
								throw new InvalidSchematicException("No potential double chest found in "+baseFile);
							}
						}
					}
				}
			}
			if(!doubleChestExists) {
				test.stopTest();
				throw new InvalidSchematicException("No potential double chest found in "+baseFile);
			}

			byte[] data = nbtdata.getByteArray("Data");

			Location l, root = spawn.clone();
			root.subtract(width/2, 1, length/2);

			chestExists = false;

			for(int y = height-1; y >= 0; y--) {
				for(int z = 0; z < length; z++) {
					for(int x = 0; x < width; x++) {

						l = root.clone().add(x, y, z);
						int index = x + (y * length + z) * width;

						Block b = l.getBlock();

						Meta.protect(b);

						b.setTypeId(blocks[index] < 0 ? Material.SPONGE.getId() : blocks[index]);
						if(blocks[index] == Material.CHEST.getId()) {
							if(!chestExists) {
								chestExists = true;
							} else {
								Chest c = (Chest) b.getState();
								chest = (DoubleChest) c.getInventory().getHolder();
							}
						}

						b.setData(data[index]);
					}
				}
			}

			makeTeamSelect(chest);

			fis.close();
		} catch (IOException e) {
			Bukkit.getLogger().log(Level.SEVERE, e.getMessage());
		} finally {
			test.stopTest();
		}
	}

	public void start(Location area) {
		Messages.sendBroadcast("start-game");
		this.area = area;

		int type = CaptureTheFlag.getPlugin().getConfig().getInt("settings.border-type");
		if(type > 0) {
			for(int i = 0; i < dimension; i++) {
				setBorder(-dimension/2, -dimension/2+i, type);
				setBorder(-dimension/2+i, dimension/2, type);
				setBorder(dimension/2-i, -dimension/2, type);
				setBorder(dimension/2, dimension/2-i, type);
			}
		}

		for(Player p : getPlayers())
			p.setCompassTarget(area);
	}

	public void stop() {

		Messages.sendBroadcast("stop-game");

		Score.save();

		for(Player p : Game.getInstance().getPlayers()) {
			Meta.removeMetadata(p, "role");
			Meta.removeMetadata(p, "invisible");
			for(PotionEffect effect : p.getActivePotionEffects()) {
                p.removePotionEffect(effect.getType());
            }
			p.setAllowFlight(false);
			p.teleport(spawn);
		}

		for(Team t : Team.getList()) {
			t.clear();
		}
		Team.getList().clear();

		stopCountdown();

		// restore bases
		teams[0].destroyBase();
		teams[1].destroyBase();

		if(area != null) {
			// regenerate chunks
			Performance test = Performance.getInstance("chunks");
			test.startTest();

			int dimension = CaptureTheFlag.getPlugin().getConfig().getInt("settings.dimension")/16+2,
					chunkX = area.getChunk().getX(),
					chunkZ = area.getChunk().getZ();

			area.getWorld().regenerateChunk(chunkX, chunkZ);
			for(int x = 1; x <= dimension/2; x++) {
				area.getWorld().regenerateChunk(chunkX-x, chunkZ);
				area.getWorld().regenerateChunk(chunkX+x, chunkZ);
				for(int z = 1; z <= dimension/2; z++) {
					area.getWorld().regenerateChunk(chunkX, chunkZ-z);
					area.getWorld().regenerateChunk(chunkX, chunkZ+z);
					area.getWorld().regenerateChunk(chunkX-x, chunkZ-z);
					area.getWorld().regenerateChunk(chunkX-x, chunkZ+z);
					area.getWorld().regenerateChunk(chunkX+x, chunkZ-z);
					area.getWorld().regenerateChunk(chunkX+x, chunkZ+z);
				}	
			}
			test.stopTest();

			area = null;
		}
	}

	public void reset() {
		new BukkitRunnable() {

			@Override
			public void run() {
				stop();

				new Team(DyeColor.WHITE);
				new Game(spawn.getWorld(), new Team(DyeColor.BLUE), new Team(DyeColor.RED));
			}
		}.runTask(CaptureTheFlag.getPlugin());
	}

	public static Game getInstance() {
		return instance;
	}

	public DoubleChest getChest() {
		return chest;
	}

	public int getDimension() {
		return dimension;
	}

	public long getId() {
		return id;
	}

	public Location getArea() {
		return area;
	}

	public Location getSpawn() {
		return spawn;
	}

	public Team[] getTeams() {
		return teams;
	}

	public Set<Schematic> getJails() {
		return jails;
	}

	public int getCountdown() {
		return countdown;
	}

	@SuppressWarnings("deprecation")
	private void setBorder(int x, int z, int type) {
		Location l = area.clone().add(x, 0, z);
		l = l.getWorld().getHighestBlockAt(l).getLocation();

		/*
		 * Do not place border on top of trees.
		 */
		while(l.getBlock().getType() == Material.LOG
				|| l.getBlock().getType() == Material.LEAVES
				|| l.getBlock().getType() == Material.AIR
				|| !l.getBlock().getType().isSolid()) {
			l.subtract(0, 1, 0);
		}
		l.getBlock().setTypeId(type);
		Meta.protect(l.getBlock());
	}

	/**
	 * Returns if the game area is already defined.
	 * 
	 * @return Status of the Game
	 */
	public boolean isRunning() {
		return area != null;
	}

	/**
	 * Returns if both bases are built.
	 * 
	 * @return Status of the Game
	 */
	public boolean isReady() {
		return isRunning() && teams[0].getBase() != null && teams[1].getBase() != null;
	}

	/**
	 * Returns if CTF is started.
	 * 
	 * @return Status of the Game
	 */
	public boolean isCTF() {
		return ctf && isReady();
	}

	public void makeTeamSelect(DoubleChest dc) {
		if(chest == null)
			chest = dc;
		for(int i = 0; i < 4; i++) {
			ItemStack is = new ItemStack(Material.LEATHER_HELMET);
			ItemMeta im = is.getItemMeta();
			List<String> lore = new ArrayList<String>();
			lore.add(WordUtils.capitalize(Role.values()[i].name().toLowerCase()));
			im.setLore(lore);
			is.setItemMeta(im);
			dc.getInventory().setItem(i, teams[0].colorizeArmor(is));
			dc.getInventory().setItem(8-i, teams[1].colorizeArmor(is));
		}
		{
			ItemStack is = new ItemStack(Material.LEATHER_CHESTPLATE);
			ItemMeta im = is.getItemMeta();
			List<String> lore = new ArrayList<String>();
			lore.add(WordUtils.capitalize(Role.PIONEER.name().toLowerCase()));
			im.setLore(lore);
			is.setItemMeta(im);
			dc.getInventory().setItem(20, teams[0].colorizeArmor(is));
			dc.getInventory().setItem(24, teams[1].colorizeArmor(is));
		}
		ItemStack potion = new ItemStack(Material.EXP_BOTTLE);
		ItemMeta im = potion.getItemMeta();
		im.setDisplayName("Invisibility");
		potion.setItemMeta(im);
		ItemStack[][] is = {
				{new ItemStack(Material.IRON_SWORD), new ItemStack(Material.BOW), new ItemStack(Material.STONE_SWORD), new ItemStack(Material.STONE_SWORD)},
				{new ItemStack(Material.IRON_CHESTPLATE), new ItemStack(Material.CHAINMAIL_CHESTPLATE), null, new ItemStack(Material.LEATHER_CHESTPLATE)},
				{new ItemStack(Material.WOOD_SPADE), new ItemStack(Material.IRON_AXE), new ItemStack(Material.IRON_PICKAXE), new ItemStack(Material.COMPASS)},
				{null, new ItemStack(Material.WOOD_SWORD), new ItemStack(Material.IRON_SPADE), potion}
		};
		for(int i = 0; i < is.length; i++) {
			for(int j = 0; j < is[i].length; j++) {
				if(is[i][j] == null)
					continue;
				im = is[i][j].getItemMeta();
				List<String> lore = new ArrayList<String>();
				lore.add(WordUtils.capitalize(Role.values()[j].name().toLowerCase()));
				im.setLore(lore);
				is[i][j].setItemMeta(im);
				dc.getInventory().setItem(9+i*9+j, is[i][j]);
				dc.getInventory().setItem(8+9+i*9-j, is[i][j]);
			}
		}
	}

	public int playersAlive() {
		int alive = 0;
		for(Player p : getPlayers()) {
			if(p.getHealth() != 0)
				alive++;
		}
		return alive;
	}

	public void giveStartBlock(Player newPlayer) {
		Messages.put("{player}", newPlayer.getDisplayName());
		Messages.sendBroadcast("choosen");

		newPlayer.getInventory().addItem(new ItemStack(Material.SPONGE));
		newPlayer.teleport(Tools.getRandomLocation(spawn.getWorld().getSpawnLocation(), 20, 50));
		newPlayer.setAllowFlight(true);
		newPlayer.setFlying(true);
		newPlayer.setFlySpeed((float) 0.4);
		Messages.sendPlayer("starting block", newPlayer);
	}	

	public void passStartBlock() {
		switch(playersAlive()) {
		case 1:
			if(Team.getTeam(0).get(0).getHealth() != 0) {
				giveStartBlock(Team.getTeam(0).get(0));	
			}
			break;
		case 0:
			break;
		default:
			int i = 0;
			do {
				i = (int) (Math.random()*getPlayers().size());
			} while(getPlayers().get(i).getHealth() == 0);
			giveStartBlock(getPlayers().get(i));
		}
	}

	public void startCountdown() {
		countdown = CaptureTheFlag.getPlugin().getConfig().getInt("settings.building-phase");
		countdownTask = new BukkitRunnable() {

			@Override
			public void run() {
				if(isReady()) {
					switch (--countdown) {
					case 10:
					case 5:
					case 4:
					case 3:
					case 2:
					case 1:
						Game.getInstance().sendMessage(countdown+"...");
						break;
					case 0:
						this.cancel();
						startCTF();
						Messages.put("{time}", String.valueOf(countdown));
						Messages.sendBroadcast("battle-begin");
						break;
					default:
						if(countdown % 15 == 0) {
							Messages.put("{time}", String.valueOf(countdown));
							Messages.sendBroadcast("build-time");
						}
						break;
					}
				} else {
					this.cancel();
					Messages.sendBroadcast("countdown-interrupted");
				}
			}
		}.runTaskTimer(CaptureTheFlag.getPlugin(), 20, 20);
	}

	private void stopCountdown() {
		if(countdownTask != null) {
			countdownTask.cancel();
		}
	}

	public void startCTF() {
		ctf = true;

		/*
		 * Set compass target
		 */
		for(Team t : teams) {
			t.setCompassTarget();
		}

		insertDatabase();
	}

	private void insertDatabase() {
		try {
			Database db = SQL.createDatabase();
			if(db.open()) {
				String prefix = CaptureTheFlag.getPlugin().getConfig().getString("database.prefix");
				if(CaptureTheFlag.getPlugin().getConfig().getBoolean("database.use-mysql")) {
					id = db.insert("INSERT INTO "+prefix +"_game (start) VALUES ('"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(System.currentTimeMillis())+"')").get(0);
				} else {
					id = db.insert("INSERT INTO "+prefix +"_game (start) VALUES ('"+Long.valueOf(System.currentTimeMillis())+"')").get(0);
				}
				db.close();
			}
		} catch(SQLException e) {
			System.out.println("Error while accessing database. Scores cannot be saved: "+e.getMessage());
		}
	}

	public void checkPlayerLocation(Location l, Player p) {
		Performance test = Performance.getInstance("border");
		test.startTest();

		if(isRunning()) {
			int	x = area.getBlockX(),
					x1 = area.getBlockX() + dimension/2, // EAST
					z1 = area.getBlockZ() + dimension/2, // SOUTH
					x2 = area.getBlockX() - dimension/2, // WEST
					z2 = area.getBlockZ() - dimension/2, // NORTH
					diff;

			boolean wallX = false,
					wallZ = false,
					leftTeam = Team.getTeamByPlayer(p).ordinal() % 2 == 1;

			// middle
			if (!isCTF()) {
				if((diff = leftTeam ? l.getBlockX()-x : x-l.getBlockX()) <= 3) {
					createWall(p, x, l.getBlockZ(), BlockFace.NORTH);
					wallX = true;
					if(diff <= 0) {
						Location r = l.clone();
						r.setX(r.getBlockX() + 0.5 + (leftTeam ? 1 : -1));
						p.teleport(r);
					}
				}
			}
			// east
			if((diff = x1-l.getBlockX()) <= 3) {
				createWall(p, x1, l.getBlockZ(), BlockFace.NORTH);
				wallX = true;
				if(diff <= 0) {
					Location r = l.clone();
					r.setX(r.getBlockX() + 0.5 - 1);
					p.teleport(r);
				}
				// west
			} else if((diff = l.getBlockX()-x2) <= 3) {
				createWall(p, x2, l.getBlockZ(), BlockFace.NORTH);
				wallX = true;
				if(diff <= 0) {
					Location r = l.clone();
					r.setX(r.getBlockX() + 0.5 + 1);
					p.teleport(r);
				}
			}
			// south
			if((diff = z1-l.getBlockZ()) <= 3) {
				createWall(p, l.getBlockX(), z1, BlockFace.EAST);
				wallZ = true;
				if(diff <= 0) {
					Location r = l.clone();
					r.setZ(r.getBlockZ() + 0.5 - 1);
					p.teleport(r);
				}
				// north
			} else if((diff = l.getBlockZ()-z2) <= 3) {
				createWall(p, l.getBlockX(), z2, BlockFace.EAST);
				wallZ = true;
				if(diff <= 0) {
					Location r = l.clone();
					r.setZ(r.getBlockZ() + 0.5 + 1);
					p.teleport(r);
				}
			}

			if(!wallX) {
				restoreWall(p, BlockFace.NORTH);
			}
			if(!wallZ) {
				restoreWall(p, BlockFace.EAST);
			}
		}

		test.stopTest();
	}

	@SuppressWarnings("deprecation")
	private void createWall(Player p, int x, int z, BlockFace f) {
		String identifier = p.getName()+f.toString();
		HashSet<Location> wall = new HashSet<Location>();
		Location l = p.getLocation().clone();
		l.setX(x);
		l.setZ(z);
		l.subtract(0, 1, 0);
		for(int i = 0; i < 3; i++) {
			l.add(0, 1, 0);
			wall.add(l.getBlock().getRelative(BlockFace.SELF).getLocation());
			wall.add(l.getBlock().getRelative(f).getLocation());
			wall.add(l.getBlock().getRelative(f).getRelative(f).getLocation());
			wall.add(l.getBlock().getRelative(f.getOppositeFace()).getLocation());
			wall.add(l.getBlock().getRelative(f.getOppositeFace()).getRelative(f.getOppositeFace()).getLocation());
		}
		HashSet<Location> restore = new HashSet<Location>();
		restore.addAll(buffer.restore(identifier));

		for(Location r : restore) {
			if(!wall.contains(r)) {
				p.sendBlockChange(r, r.getBlock().getType(), r.getBlock().getData());
				buffer.remove(identifier, r);
			}
		}
		for(Location r : wall) {
			if(!restore.contains(r) && !r.getBlock().getType().isSolid()) {
				buffer.save(identifier, r);
				p.sendBlockChange(r, Material.THIN_GLASS, (byte) 0);
			}
		}
	}

	@SuppressWarnings("deprecation")
	private void restoreWall(Player p, BlockFace f) {
		String identifier = p.getName()+f.toString();
		HashSet<Location> restore = new HashSet<Location>();
		restore.addAll(buffer.restore(identifier));
		for(Location r : restore) {
			p.sendBlockChange(r, r.getBlock().getType(), r.getBlock().getData());
			buffer.remove(identifier, r);
		}
	}

	public boolean inArea(Location l) {
		int	x = l.getBlockX(),
				z = l.getBlockZ(),
				x1 = area.getBlockX() + dimension/2,
				z1 = area.getBlockZ() + dimension/2,
				x2 = area.getBlockX() - dimension/2,
				z2 = area.getBlockZ() - dimension/2;
		return x < x1 && x > x2 && z < z1 && z > z2;
	}

	public void joinCTF(Player player) {
		try {
			backup.save(player);
		} catch (IOException e) {
			String msg = ChatColor.DARK_PURPLE+"Unable to save inventory: "+e.getMessage();
			Bukkit.getLogger().severe(msg);
			player.sendMessage(msg);
			return;
		}
		try {
			player.setGameMode(GameMode.valueOf(CaptureTheFlag.getPlugin().getConfig().getString("settings.gamemode")));
		} catch (IllegalArgumentException e) {
			String msg = ChatColor.DARK_PURPLE+"Unable to set game mode: "+e.getMessage();
			Bukkit.getLogger().severe(msg);
			player.sendMessage(msg);
			return;
		}
		player.setFoodLevel(20);
		player.setHealth(player.getMaxHealth());
		player.setTotalExperience(0);
		player.setLevel(0);
		player.setExp(0);

		Team.getTeam(0).add(player);
		Score.add(player);
		player.setCompassTarget(spawn);

		if(!isRunning() && playersAlive() == 1) {
			giveStartBlock(player);
		} else {
			// If the area exists or not first player
			player.teleport(spawn);
			if(isRunning())
				Messages.sendPlayer("select-team", player);
			else
				Messages.sendPlayer("wait-for-game", player);
		}
	}

	public void leaveCTF(Player player) {
		final Team t = Team.getTeamByPlayer(player),
				u = t.getEnemyTeam();

		if(t.ordinal() != 0 && Game.getInstance().isCTF()) {
			if (t.size() == 2 && u.size() >= 2) {
				// team size falls down to 1
				Messages.put("{team}", t.colorizeText());
				Messages.sendBroadcast("game-paused");
			} else {
				Team.dropFlag(player);
			}
		}

		boolean wasStarter = !Game.getInstance().isRunning() && player.getInventory().contains(Material.SPONGE);

		Team.getTeam(0).add(player);
		Team.getTeam(0).remove(player);

		try {
			backup.restore(player);
		} catch (IOException e) {
			String msg = ChatColor.DARK_PURPLE+"Error while loading inventory: "+e.getMessage();
			Bukkit.getLogger().severe(msg);
			player.sendMessage(msg);
			player.teleport(Bukkit.getWorlds().get(0).getSpawnLocation());
			player.setGameMode(Bukkit.getServer().getDefaultGameMode());
			player.setFoodLevel(20);
			player.setHealth(20);
			player.setTotalExperience(0);
			player.setLevel(0);
			player.setExp(0);
		}

		if(wasStarter) {
			player.setAllowFlight(false);
			Game.getInstance().passStartBlock();
		}

		if(totalPlayers() == 0) {
			// reset game if teams are empty
			Game.getInstance().reset();
		}
	}

	public boolean playsCTF(Player player) {
		return spawn.getWorld().equals(player.getWorld());
	}

	public void sendMessage(String message) {
		for(Team t : Team.getList()) {
			t.sendMessage(message);
		}
	}

	public List<Player> getPlayers() {
		Performance test = Performance.getInstance("list");
		test.startTest();
		ArrayList<Player> list = new ArrayList<Player>();
		for(Team t : Team.getList()) {
			list.addAll(t);
		}
		test.stopTest();
		return list;
	}

	public int totalPlayers() {
		return getPlayers().size();
	}

}
