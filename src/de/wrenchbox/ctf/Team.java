package de.wrenchbox.ctf;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import net.minecraft.server.v1_8_R1.NBTCompressedStreamTools;
import net.minecraft.server.v1_8_R1.NBTTagCompound;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.block.DoubleChest;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.material.Wool;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import de.wrenchbox.ctf.Exceptions.InvalidSchematicException;
import de.wrenchbox.ctf.Util.Messages;
import de.wrenchbox.ctf.Util.Meta;
import de.wrenchbox.ctf.Util.Performance;

@SuppressWarnings({"serial", "deprecation"})
public class Team extends ArrayList<Player> {

	private int points = 0;
	private DyeColor color;
	private ItemStack[] restoreInventory;
	private Location base, playerRestore;
	private short width, height, length;
	private BlockState[] lastState;
	private Block flag;
	private BukkitTask flagTask;
	private DoubleChest chest;
	private static ArrayList<Team> list = new ArrayList<Team>(3);

	public Team(DyeColor color) {
		this.color = color;
		list.add(this);
	}

	public DyeColor getColor() {
		return color;
	}

	public byte getWoolData() {
		return color.getWoolData();
	}

	public ItemStack colorizeArmor(ItemStack armor) {
		LeatherArmorMeta lam = (LeatherArmorMeta) armor.getItemMeta();
		lam.setColor(color.getColor());
		armor.setItemMeta(lam);
		return armor;
	}

	public void setBase(Location l) {
		base = l;
	}

	public Location getBase() {
		return base;
	}

	public Location getSpawn() {
		if(Team.indexOf(this) == 0)
			return base;
		Location l = base.clone();
		return l.add(-1, 0, -1);
	}

	public Block getFlag() {
		return flag;
	}

	public void setPlayerRestore(Location l) {
		playerRestore = l;
	}

	public Location getPlayerRestore() {
		return playerRestore;
	}

	public String colorizeChat(String text) {
		return ChatColor.valueOf(color.name())+text+ChatColor.RESET;
	}

	public String colorizeText(Player p) {
		return colorizeChat(p.getName());
	}

	public String colorizeText() {
		return colorizeChat(toString());
	}

	public String colorizePoints() {
		return colorizeChat(Integer.toString(points));
	}

	@Override
	public boolean add(Player p) {
		Team t = Team.getTeamByPlayer(p);
		try {
			if(ordinal() == 0) {
				if (t != null) {
					Score.save(p);
				}
			} else {
				Score.load(p, this);
			}
		} catch (SQLException e) {
			Bukkit.getLogger().severe("Error while accessing database. Scores for player '"+p.getName()+"' cannot be saved/loaded: "+e.getMessage());
		}

		if (t != null) {
			t.remove(p);
			t.saveLoot(p);
		}
		p.setPlayerListName(ChatColor.valueOf(color.name())+(p.getName().length() > 14 ? p.getName().substring(0, 14) : p.getName()));
		p.setDisplayName(colorizeText(p));
		p.getInventory().setArmorContents(null);
		p.getInventory().clear();
		p.setHealth(20);
		return super.add(p);
	}

	public void saveLoot(Player p) {
		if(chest != null) {
			int offset = 0;
			switch((Role) Meta.getMetadata(p, "role").value()) {
			case FIGHTER:
				offset = 2;
				break;
			case PIONEER:
			case SPY:
				offset = 3;
				break;
			case ARCHER:
				offset = 4;
				break;
			default:
				break;
			}
			for(int i = offset; i < p.getInventory().getContents().length; i++) {
				ItemStack is = p.getInventory().getContents()[i];
				if(is != null)
					chest.getInventory().addItem(is);
			}
		}
	}

	/**
	 * Loads the base from schematics file depending on the team number.
	 * 
	 * @param player Player who placed the block and is the founder of the base.
	 */
	public void loadBase(Location root) throws InvalidSchematicException {
		Performance test = Performance.getInstance("base");
		test.startTest();

		try {
			String baseFile = CaptureTheFlag.getPlugin().getDataFolder().getPath()+"/schematics/base"+ordinal()+".schematic";
			FileInputStream fis = new FileInputStream(baseFile);
			NBTTagCompound nbtdata = NBTCompressedStreamTools.a(fis);


			width = nbtdata.getShort("Width");
			height = nbtdata.getShort("Height");
			length = nbtdata.getShort("Length");

			byte[] blocks = nbtdata.getByteArray("Blocks");

			int chestX = 0, chestZ = 0;
			boolean flagExists = false,
					chestExists = false,
					doubleChestExists = false;
			for(byte b : blocks) {
				if(b == Material.WOOL.getId()) {
					if(!flagExists) {
						flagExists = true;
					} else {
						test.stopTest();
						throw new InvalidSchematicException("More than one potential flags found in "+baseFile);
					}
				}
			}
			if(!flagExists) {
				test.stopTest();
				throw new InvalidSchematicException("No potential flag found in "+baseFile);
			}

			for(int y = 0; y < height; y++) {
				for(int z = 0; z < length; z++) {
					for(int x = 0; x < width; x++) {

						int index = x + (y * length + z) * width;
						byte b = blocks[index];

						if(b == Material.CHEST.getId()) {
							if(!chestExists) {
								chestExists = true;
								chestX = x;
								chestZ = z;
							} else if(chestExists && !doubleChestExists && Math.abs(x-chestX)+Math.abs(z-chestZ) == 1) {
								doubleChestExists = true;
							} else if(doubleChestExists) {
								test.stopTest();
								throw new InvalidSchematicException("More than one double chests found in "+baseFile);
							} else {
								test.stopTest();
								throw new InvalidSchematicException("No potential double chest found in "+baseFile);
							}
						}
					}
				}
			}
			if(!doubleChestExists) {
				test.stopTest();
				throw new InvalidSchematicException("No potential double chest found in "+baseFile);
			}

			byte[] data = nbtdata.getByteArray("Data");

			Location l = root.clone();
			l.subtract(width/2, 1, length/2);

			lastState = new BlockState[width*height*length];

			chestExists = false;

			for(int y = height-1; y >= 0; y--) {
				for(int z = 0; z < length; z++) {
					for(int x = 0; x < width; x++) {
						int index = x + (y * length + z) * width;

						Block b = l.clone().add(x, y, z).getBlock();

						Meta.protect(b);

						lastState[index] = b.getState();

						b.setTypeId(blocks[index] < 0 ? Material.SPONGE.getId() : blocks[index]);
						if(blocks[index] == Material.WOOL.getId()) {
							b.setData(getWoolData());
							flag = b;
							Meta.setMetadata(flag, "position", flag);
						} else { 
							if(blocks[index] == Material.CHEST.getId()) {
								if(!chestExists) {
									chestExists = true;
								} else {
									Chest c = (Chest) b.getState();
									chest = (DoubleChest) c.getInventory().getHolder();
								}
							}
							b.setData(data[index]);
						}
					}
				}
			}
			fis.close();
		} catch (IOException e) {
			width = 5;
			height = 4;
			length = 5;
			lastState = new BlockState[width*height*length];
			Material floor = root.clone().subtract(0, 1, 0).getBlock().getType().isSolid() ? root.clone().subtract(0, 1, 0).getBlock().getType() : Material.WOOD;
			byte floorData = root.clone().subtract(0, 1, 0).getBlock().getData();
			Chest c = null;
			for(int y = height-1; y >= 0; y--) {
				for(int z = 0; z < length; z++) {
					for(int x = 0; x < width; x++) {
						Block b = root.clone().add(x-width/2, y-1, z-length/2).getBlock();
						int index = x + (y * length + z) * width;
						lastState[index] = b.getState();
						b.setType(y == 0 ? floor : Material.AIR);
						b.setData(y == 0 ? floorData : 0);

						if(y == 1 && z == 0 && (x == 0 || x == 1)) {
							b.setType(Material.CHEST);
							b.setData((byte) BlockFace.NORTH.ordinal());
							if(c == null) {
								c = (Chest) b.getState();
							}
						}

						Meta.protect(b);
					}	
				}
			}
			chest = (DoubleChest) c.getInventory().getHolder();
			Block b = root.getBlock();
			b.setType(Material.FENCE);
			b = root.clone().add(0, 1, 0).getBlock();
			b.setType(Material.WOOL);
			b.setData(getWoolData());
			flag = b;
			Meta.setMetadata(flag, "position", flag);
		}
		test.stopTest();
	}

	/**
	 * Creates a base from schematics file depending on the team number.
	 * 
	 * @param player Player who placed the block and is the founder of the base.
	 * @param base	The block that is the home stone of the base.
	 * @throws InvalidSchematicException Thrown if there is no/more than one potential flag in a base.
	 */
	public void foundBase(Player player, Block baseBlock) throws InvalidSchematicException {
		/*
		 * Build the base and save the last state of the environment.
		 */
		baseBlock.setType(Material.AIR);
		loadBase(baseBlock.getLocation());
		base = baseBlock.getLocation();
		playerRestore = player.getLocation();

		/*
		 * Get direction of the entry and adjust spawn
		 */
		BlockFace direction = BlockFace.values()[(Math.round(player.getLocation().getYaw()/90)+6) % 4]; // Normalized to fit BlockFace constants
		base.setPitch(0);
		base.setYaw(direction.ordinal()*90 % 360);

		/*
		 *  Teleport players to not get stuck in walls.
		 *  Remove wool from all team player's inventory.
		 */

		double distBaseSquare = Math.pow(width > length ? width : length, 2);
		Location l = base.clone().subtract(0.5, 0, 0.5);
		for(Player q : this) {
			if(base.distanceSquared(q.getLocation()) <= distBaseSquare){
				l.setPitch(q.getLocation().getPitch());
				l.setYaw(q.getLocation().getYaw());
				q.teleport(l);
			}
			q.getInventory().remove(Material.WOOL);
		}

		/*
		 * Inform players about the base.
		 */
		Messages.put("{player}", player.getDisplayName());
		Messages.put("{coords}", base.getBlockX()+" : "+base.getBlockY()+" : "+base.getBlockZ());
		Messages.sendTeam("found-base", this);
		Messages.put("{team}", colorizeText());
		Messages.sendTeam("found-base-enemy", getEnemyTeam());
	}	

	/**
	 * Remove the base and restore the last state.
	 */
	public void destroyBase() {
		/*
		 * Skip if there is no base.
		 */
		if(base == null)
			return;

		/*
		 * Remove the base and restore the last state. 
		 */
		Meta.removeMetadata(flag, "position");

		for(int y = 0; y < height; y++) {
			for(int z = 0; z < length; z++) {
				for(int x = 0; x < width; x++) {
					int index = x + (y * length + z) * width;
					lastState[index].update(true);

					// Remove protection
					Meta.unprotect(lastState[index].getBlock());
				}
			}
		}

		/*
		 * Teleport players in this area to the saved location to avoid getting stuck in the wall.
		 * Add wool to found a new base.
		 */

		double distBaseSquare = Math.pow(width > length ? width : length, 2);
		if(Game.getInstance().isRunning()) {
			for(Player q : Game.getInstance().getPlayers()) {
				if(base.distanceSquared(q.getLocation()) <= distBaseSquare) {
					q.teleport(playerRestore);
				}
				if(contains(q)) {
					q.getInventory().addItem(new Wool(color).toItemStack(1));
					q.updateInventory();
				}
			}
		}
		base = null;
		playerRestore = null;
	}

	public void sendMessage(String message) {
		for(Player p : this) {
			p.sendMessage(message);
		}
	}

	public String toString() {
		return color.name().substring(0, 1)+color.name().substring(1).toLowerCase();
	}

	public static Team getTeamByPlayer(Player p) {
		for(Team t : list) {
			if(t.contains(p))
				return t;
		}
		return null;
	}

	public Team getEnemyTeam() {
		int i = ordinal();
		return i == 0 ? this : getTeam(i - (i+1) % 2 + i % 2);
	}

	public void stealFlag(Player player) {
		if(flagTask != null) {
			flagTask.cancel();
		}

		ArrayList<Player> list = new ArrayList<Player>(this);
		list.addAll(getEnemyTeam());

		for(Player p : list) {
			p.playSound(p.getLocation(), Sound.WITHER_SPAWN, 1, 1);
		}

		restoreInventory = player.getInventory().getArmorContents();
		ItemStack[] is = {
				getEnemyTeam().colorizeArmor(new ItemStack(Material.LEATHER_BOOTS)),
				getEnemyTeam().colorizeArmor(new ItemStack(Material.LEATHER_LEGGINGS)),
				getEnemyTeam().colorizeArmor(new ItemStack(Material.LEATHER_CHESTPLATE)),
				new Wool(color).toItemStack(1)
		};
		player.getInventory().setArmorContents(is);
		player.updateInventory();

		// save position of flag(player)
		Meta.setMetadata(flag, "position", player);
	}

	public void returnFlag() {

		if(Meta.getMetadata(flag, "position") != null) {
			if(Meta.getMetadata(flag, "position").value() instanceof Player) {
				// a player has the flag

				Player player = (Player) Meta.getMetadata(flag, "position").value();

				restoreInventory(player);

				// restore flag
				flag.setType(Material.WOOL);
				flag.setData(getWoolData());

				// save position of flag(base)
				Meta.setMetadata(flag, "position", flag);
			} else if(Meta.getMetadata(flag, "position").value() instanceof Block) {
				// the flag was dropped somewhere

				if(flagTask != null) {
					flagTask.cancel();
				}

				Block f = (Block) Meta.getMetadata(flag, "position").value();
				Meta.unprotect(f);
				f.setType(Material.AIR);

				// restore flag
				flag.setType(Material.WOOL);
				flag.setData(getWoolData());

				// save position of flag(base)
				Meta.setMetadata(flag, "position", flag);

				// reward player with flag
				Player player = Meta.getAncient(this);

				if(player != null) {
					int dist = Math.abs(player.getLocation().getBlockX()-flag.getX()),
							temp = Math.abs(player.getLocation().getBlockY()-flag.getY());
					if(temp > dist)
						dist = temp;
					temp = Math.abs(player.getLocation().getBlockZ()-flag.getZ());
					if(temp > dist)
						dist = temp;

					if(dist <= 3) {
						earnPoint(player);
					}
				}
			}
		}
	}

	public void restoreInventory(Player player) {
		player.getInventory().setArmorContents(restoreInventory);
		player.updateInventory();
	}

	public void earnPoint(Player player) {
		points++;
		getEnemyTeam().returnFlag();
		int earnExp = CaptureTheFlag.getPlugin().getConfig().getInt("settings.return-flag-exp");
		Score.get(player).incPoints(earnExp);
		player.giveExp(earnExp);

		ArrayList<Player> list = new ArrayList<Player>(this);
		list.addAll(getEnemyTeam());

		for(Player p : list) {
			p.playSound(p.getLocation(), Sound.WITHER_DEATH, 1, 1);
		}

		Messages.put("{player}", player.getDisplayName());
		Messages.put("{own}", colorizePoints());
		Messages.put("{enemy}", getEnemyTeam().colorizePoints());
		Messages.sendBroadcast("earn-point");
	}

	public void setCompassTarget(final Player player) {
		new BukkitRunnable() {

			@Override
			public void run() {
				if(Game.getInstance().isCTF()) {
					player.setCompassTarget(getEnemyTeam().getBase());
				} else {
					player.setCompassTarget(Game.getInstance().getArea());
				}
			}
		}.runTask(CaptureTheFlag.getPlugin());
	}

	public void setCompassTarget() {
		for(Player p : this)
			setCompassTarget(p);
	}

	public int ordinal() {
		return list.indexOf(this);
	}

	@Override
	public boolean equals(Object o) {
		if(o == null || !(o instanceof Team)) {
			return false;
		} else {
			return ((Team) o).getColor().equals(color); 
		}
	}

	private void startFlagTimer() {
		flagTask = Bukkit.getScheduler().runTaskLater(CaptureTheFlag.getPlugin(), new Runnable() {

			@Override
			public void run() {
				Messages.put("{team}", colorizeText());
				Messages.sendBroadcast("flag returned");
				returnFlag();
			}
		}, 20*CaptureTheFlag.getPlugin().getConfig().getInt("settings.return-flag"));
	}

	public static void dropFlag(Player p) {
		Team t = getTeamByPlayer(p);
		if (Meta.isAncient(p)) {
			// drop flag
			t.restoreInventory(p);
			
			Team u = t.getEnemyTeam();

			Block b = p.getLocation().getBlock();
			b.setType(Material.WOOL);
			b.setData(u.getWoolData());

			// save position of flag(dropzone)
			Meta.protect(b);
			Meta.setMetadata(u.getFlag(), "position", b);
			Meta.setMetadata(b, "capture-delay", System.currentTimeMillis()
					+ CaptureTheFlag.getPlugin().getConfig().getInt("settings.capture-delay")*1000);

			u.startFlagTimer();
		}
	}

	public static Team getTeam(int i) {
		return list.get(i);
	}

	public static int indexOf(Team t) {
		return list.indexOf(t);
	}

	public static ArrayList<Team> getList() {
		return list;
	}
}
