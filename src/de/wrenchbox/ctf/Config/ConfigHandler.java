package de.wrenchbox.ctf.Config;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import de.wrenchbox.ctf.CaptureTheFlag;

public class ConfigHandler {

	private File messagesfile;
	private static FileConfiguration messageconfig;

	public static CaptureTheFlag plugin;
	public ConfigHandler(CaptureTheFlag instance) {
		plugin = instance;
		loadDefaultConfig();
		reloadMessageConfig();
		addMessageDefaults();
	}

	public void loadDefaultConfig() {
		plugin.getConfig().addDefault("settings.allow-sprinting", false);
		plugin.getConfig().addDefault("settings.border-type", 7);
		plugin.getConfig().addDefault("settings.building-phase", 300);
		plugin.getConfig().addDefault("settings.capture-delay", 5);
		plugin.getConfig().addDefault("settings.change-team-range", 4);
		plugin.getConfig().addDefault("settings.dimension", 100);
		plugin.getConfig().addDefault("settings.force-ctf", false);
		plugin.getConfig().addDefault("settings.gamemode", "ADVENTURE");
		plugin.getConfig().addDefault("settings.respawn", 10);
		plugin.getConfig().addDefault("settings.respawn-no-dmg", 3);
		plugin.getConfig().addDefault("settings.return-flag", 30);
		plugin.getConfig().addDefault("settings.return-flag-exp", 17);
		plugin.getConfig().addDefault("settings.return-flag-on-click", true);
		plugin.getConfig().addDefault("settings.vote", 60);
		plugin.getConfig().addDefault("settings.world", "openctf");
		plugin.getConfig().addDefault("settings.cooldown.invisible", 10);

		// SQL config
		plugin.getConfig().addDefault("database.use-mysql", false);
		plugin.getConfig().addDefault("database.hostname", "localhost");
		plugin.getConfig().addDefault("database.port", 3306);
		plugin.getConfig().addDefault("database.database", "dbname");
		plugin.getConfig().addDefault("database.user", "username");
		plugin.getConfig().addDefault("database.password", "password");
		plugin.getConfig().addDefault("database.prefix", "ctf");

		plugin.getConfig().options().copyDefaults(true);
		plugin.saveConfig();
	}

	public void reloadMessageConfig() {
		messagesfile = new File(plugin.getDataFolder(), "messages.yml");
		messageconfig = YamlConfiguration.loadConfiguration(messagesfile);
	}

	public void addMessageDefaults() {
		messageconfig.addDefault("broadcast-messages.battle-begin", "&eThe battle has begun!");
		messageconfig.addDefault("broadcast-messages.build-time", "&eBuilding phase ends in {time} seconds...");
		messageconfig.addDefault("broadcast-messages.choosen", "{player} was choosen to select a new area.");
		messageconfig.addDefault("broadcast-messages.countdown-interrupt", "&cCountdown interrupted. Please place both bases to start the game.");
		messageconfig.addDefault("broadcast-messages.earn-point", "{player} &ereturned the flag: {own}&e:{enemy}");
		messageconfig.addDefault("broadcast-messages.flag-capture", "{player} &ecaptured {team}&e's flag.");
		messageconfig.addDefault("broadcast-messages.flag-returned", "{team}&e's flag returned to base.");
		messageconfig.addDefault("broadcast-messages.game-paused", "{team} has less then 2 players. You cannot capture flags anymore.");
		messageconfig.addDefault("broadcast-messages.game-started", "&eArea was set and the game has been started. Please select a team and role from the chest.");
		messageconfig.addDefault("broadcast-messages.player-kicked-reason", "{target} &ewas kicked: {reason}");
		messageconfig.addDefault("broadcast-messages.player-kicked", "{target} &ewas kicked.");
		messageconfig.addDefault("broadcast-messages.prepare for game", "&eBe prepared! The game starts in {time} seconds. To interrupt the countdown pick up your base again.");
		messageconfig.addDefault("broadcast-messages.restart-vote", "{player} &ehas issued a voting: Restart");
		messageconfig.addDefault("broadcast-messages.restart-vote failed", "&eVoting failed: Restart.");
		messageconfig.addDefault("broadcast-messages.start-game", "&eStarting game...");
		messageconfig.addDefault("broadcast-messages.stop-game", "&eStopping game...");
		messageconfig.addDefault("broadcast-messages.countdown-interrupted", "&eCountdown interrupted. Both bases have to be build in order to start the game.");
		messageconfig.addDefault("broadcast-messages.votekick-reason", "{player} &ehas issued a voting: Kick {target}&e; Reason: {reason}");
		messageconfig.addDefault("broadcast-messages.votekick", "{player} &ehas issued a voting: Kick {target}");
		messageconfig.addDefault("broadcast-messages.vote-call", "&eType &b/yes &eor &b/no &eto vote. Time left: &5{time}");
		messageconfig.addDefault("broadcast-messages.votekick-failed", "&eVoting failed: Kick {target}");
		messageconfig.addDefault("broadcast-messages.votekick-reason failed", "&eVoting failed: Kick {target}&e; Reason: {reason}");

		messageconfig.addDefault("team-messages.found-base", "{player}&e founded your base at {coords}");
		messageconfig.addDefault("team-messages.found-base-enemy", "&eTeam {team} &efounded its base.");
		messageconfig.addDefault("team-messages.player-teamjoin", "{player} &ejoined team {team} &eas &f{role}");
		messageconfig.addDefault("team-messages.player-teamjoin-enemy", "{player} &ejoined team {team}");
		
		messageconfig.addDefault("player-messages.be-fair", "&ePlease be fair and support team {team}.");
		messageconfig.addDefault("player-messages.capture-forbidden", "&eYou cannot capture flags while one team has less than 2 players.");
		messageconfig.addDefault("player-messages.change-dead", "&eYou cannot change team/class while beeing dead.");
		messageconfig.addDefault("player-messages.change-with-flag", "&eYou cannot change team/class, as long as you carry the flag.");
		messageconfig.addDefault("player-messages.change-range", "&eYou need to be in your base to change team/class.");
		messageconfig.addDefault("player-messages.cooldown", "&eAbility not ready yet. {seconds} seconds left.");
		messageconfig.addDefault("player-messages.not-participating", "&eYou are not participating in CTF.");
		messageconfig.addDefault("player-messages.one-vote-only", "&cYou can only vote once every voting.");
		messageconfig.addDefault("player-messages.protected", "&eThis is a protected area. You cannot build here.");
		messageconfig.addDefault("player-messages.respawn", "&eRespawned!");
		messageconfig.addDefault("player-messages.respawn-time", "&eRespawn in {time} seconds...");
		messageconfig.addDefault("player-messages.select-team", "&ePlease select a team and role by opening the chest.");
		messageconfig.addDefault("player-messages.sprint", "&eSprinting is turned off.");
		messageconfig.addDefault("player-messages.starting block", "&eYou have got the starting block(Sponge). Please set a game area by flying wherever you want and placing the block.");
		messageconfig.addDefault("player-messages.near-border", "&eToo close to the border. Go back!");
		messageconfig.addDefault("player-messages.vote-choice", "&aYou voted with {choice}");
		messageconfig.addDefault("player-messages.vote-running", "&eThere is already running a voting. Type &b/yes &eor &b/no &eto vote.");
		messageconfig.addDefault("player-messages.wait-for-game", "&ePlease wait while the game is beeing created.");
		messageconfig.addDefault("player-messages.wait-capture-delay", "&eYou have to wait &c{seconds}s&e.");

		messageconfig.addDefault("general-messages.kicked", "You were kicked");
		messageconfig.addDefault("general-messages.build-error", "&5Failed to build base. Please contact the server administrator: {error}");
		messageconfig.options().copyDefaults(true);
		try {
			messageconfig.save(messagesfile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static FileConfiguration getMsgConfig() {
		return messageconfig;
	}
}
